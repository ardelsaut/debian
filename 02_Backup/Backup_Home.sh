#!/bin/bash

clear

dossierBackup="/mnt/nas/archives/PC/Linux/Backup_Home"

if [ ! -d "$dossierBackup" ]; then
    mkdir -p "$dossierBackup"
fi

if [ ! -f "$dossierBackup/home_$(date +'%d_%m_%Y').tar" ]; then
    rm -rf "$dossierBackup/home_$(date +'%d_%m_%Y').tar"
fi

tar --exclude="$HOME/.cache" --exclude="$HOME/Documents/virtualbox" --exclude="$HOME/.local/share/Trash/info"  --exclude="$HOME/.local/share/Trash/files" --use-compress-program=pigz --sort=name -cvf $dossierBackup/home_$(date +'%d_%m_%Y').tar $HOME
# --exclude="$HOME/.steam"
 sleep 3

num_tar_files=$(ls -lt "$dossierBackup"/*.tar 2>/dev/null | wc -l)
if [ $num_tar_files -gt 3 ]; then
    oldest_tar_file=$(ls -lt "$dossierBackup"/*.tar | awk '{print $NF}' | tail -1)
    sudo rm -rvf "$oldest_tar_file"
fi







# # check si le fichier existe
# file="/home/nono/.config/NonoOS/backup/test.log"
# if [ -e "$file" ]; then
#     modification_time=$(stat -c %Y "$file")
#     current_time=$(date +%s)
#     time_diff=$((current_time - modification_time))
#     three_days=$((3 * 24 * 60 * 60))

#     if [ "$time_diff" -gt "$three_days" ]; then
#         rm "$file"
#         echo "Le fichier $file a été supprimé car il est plus vieux que 3 jours."
#         echo "Lancement de la mise a jour du backup config home"
#         FaireBackup="oui"
#     else
#         echo "La mise a jour s'est faite endéans des 3jours. Pas de mise à jour du backup à faire."
#         FaireBackup="non"
#         echo "faut-il continuer (y/n)"
#         read -r -t 5 -n 1 response  # Wait for 5 seconds for user input
#         echo  # Add newline after user input
#         response=$(echo "$response" | tr '[:upper:]' '[:lower:]')
#         if [[ "$response" =~ ^(y|1|oui|o|yes)$ ]]; then
#                 FaireBackup="oui"
#         else
#             echo "Pas de backup à lancer..."
#         fi
#     fi
# else
#     echo "Le fichier $file n'existe pas."
#     echo "Creation du fichier"
#     touch $file
#     FaireBackup="oui"

# fi


# if [ "$FaireBackup" = "oui" ]; then
    
#     if [ -d "$HOME/.local/share/Trash/" ]; then
#         sudo rm -rf "$HOME"/.local/share/Trash/*
#     fi

# fi