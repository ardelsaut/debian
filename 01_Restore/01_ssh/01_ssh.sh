#!/bin/bash

# Générer clé publique : 'ssh-keygen -t ed25519'

clear

# Installation de ssh, si nécessaire
if ! which ssh > /dev/null 2>&1; then
    echo "ssh n'est pas installé"
    echo "Installation de ssh, un peu de patience..."
    sudo apt-get update > /dev/null 2>&1
    sudo apt-get install ssh -y --no-install-recommends > /dev/null 2>&1
    sudo apt-get install sshfs -y --no-install-recommends > /dev/null 2>&1
    echo "ssh est installé!"
    echo "---"
fi

# Creation du dossier '.ssh', si nécessaire
ssh_dir="$HOME/.ssh"
if [ ! -d "$ssh_dir" ]; then
    echo "Création du dossier $ssh_dir"
    mkdir -p "$ssh_dir"
    echo "dossier '.ssh' créé!"
    echo "---"
fi

# Création di fichier 'authorized_keys' si nécessaire
authorized_keys="$ssh_dir/authorized_keys"
if [ ! -f "$authorized_keys" ]; then
    echo "Création du fichier $authorized_keys"
    touch "$authorized_keys"
    echo "fichier $authorized_keys créé!"
    echo "---"

fi

# Clés 'PC-Fixe'
if ! grep -q "nono@PC-Fixe" "$authorized_keys"; then 
    echo "Installation de la clé d'authentification pour 'nono@PC-Fixe'"
    echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCpTJX1DAgnUjC22cMKJTP5ePguOKyDjAs7wV3EeEsFHA51Sz/2/FMSjdyS6OOhLv63QsdGpi/kS8KK4nF7k6ODDzov5ki2UB45VJ7lFXqWbSIywUrEDr2VVrckStuFwUz71kD9OvK4qMV6eMHin9ynD2YhQUmC5ss6AJF98ldVk4nU+xsMy5ILXewJe+XmUeuyx6v8jPaJnaIkcKbsmIqfD/yokxNCYtEWZZSMWTsM2fTVgiMCgqJrcXfV40Y/Gpp35cKRLp4E2HuV4jthKi7p+Tcwjd+SfeWiAFecdW245/tobazf8PmPwwrDUAyUj+TOpJ1gx6zso2gE8kxJuLT1rTUSDrwuTNyNCFTT1g8olHDZgfEz8qRN5+ilpZTBZA7uyjkLWAj5P/3zZFT75T2zvXqKSI+/xrQeFvQOE7LlylU/tTqRa+TZZkGczrJ3zFshv79jtRaBsBhyyucwdfQR5V/l7lSbmyRbNmGmYtasR/QsQ/oSZqJLh22gzLpozPU= nono@PC-Fixe
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDxtl6lK9nKpK93otiNmCrLgpXqKNW5uhlRjfJ/Zlqojadm55SZ8BnbckqtZo7rdscBA+8+bQAtmltVI4enIOkDs4CHO5UMewnlyNgmVoEzmG3mstlXgvc+UB8OubCUw3bp4+9GJcxx7fOzQpwUz3KGoJU4QxssA7U2AXNt0xT6kK2/S3hFRQb334x8plXD2sLfr0Th6GtLiyDhjIrVdGmXVywQJWmiR86vXUWDcWmU9OAK+5fn2+d+1a8pFykaZACIZBx6unxik+7/9Wq5JEHJqfJ1sLkUFBc9aEZWf3gNIZbSFzNIa2GQegqH9kCGQHFwgwfeFevYUcDkosZzjcOi2+d9aK9B7493zLX87UxMfmTKX6tma6rmiixBnsj+Ch9ZZi5ElhzxXg3QDsdXPbr124se125bU8MS39cx21dFEVR6IWCII7Z3PX5tGZwYxyh0NTJPyKV1lcvLp6vcFm4y0GtcFX77cu0CQOMf+l1jKtq8tSxXBElKpUuxhc0SmJM= nono@PC-Fixe' >> "$authorized_keys"
    echo "clés pc-fixe autorisées"
    echo "---"
fi

# Clés pc-portable
if ! grep -q "nono@debian-portable" "$authorized_keys"; then 
    echo "Installation de la clé d'authentification pour 'nono@debian-portable'"
    echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCYkzOBG0w9Rsaptrx0fbDY9RizZtr4U+XuplGy90mpLvnVTtSGy6ChPsu0hbq19H407kUbkiTN5YXN51AtUKGro8aCHWuDrmECKc8bxq+CoeeD1JnaeYw55rbXIsGAMtTpFHnSrHKNtGn3xlWp2U6SBYczP6XjX90LRENKCCEZZMMfo201g670JDK10OnK8Pu3IUO1VlQilwUT4yYf3aV+J1lJ7KyabUhlESNgtIiKy2lWwW/DLVZ39mZ7H1XQN1lajO/XkHKcFuylTe10PNNZRLP4CO48HpSz4xf7d0DEJ2kjzWLxFYbVGo3ZIoBpgAXhR65k1Vi0nr9pf29xvrspcMolzNyFIBuYlUFIrRaCr8mU4+HY5dKMuS4sKdwO6OQ+yPEnZvBTasSpjl/0oGXnysU4PL1ud+HqY5O5GJDxRrs1BM6eIjtv4130ZdsnWQGpXeKsafHoOG568aFN4n/xi3b3pbJ1as97NwHdlDD3bUqssFe9i6MwIxxYv7hR0rbFvG8ZFWWY3FCorf1l4CdpdNKmD+iW27kkKJFHExXL79vPO7+CmXtp6OHWT8QVgWkeYxxOGa5LZm6w+hQwTBFORE21Q6t0gX9+92Igm6OWtOIc+jeSuxYM4nTEryNnGvJYka4Ah82ConeZH7/pSnNHKBkWJA4NedXPnQ1IeRjQfw== nono@debian-portable
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGXxmWsb7byzBVhhrAXvTpbT/9mFw7oHFuaSn6xdAc7o nono@debian-portable
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDkF/chjInKBzTl3GFsZ2EbKrXQjq1/LUhpxhhmPE2FFh/Smj0sSC+TFh36HzeKgTgyhdCJ3wpXAMBvie9Q9zHNrI86iCs6DkETK/hXQ8QRI0YP6C71fQO+KSO4VUZp+zEbnFIHxTA0/gdR2Fg9hG+779e3kaCbA4MWnCG/mXpemdsT3440RHbyxWs+TOzOLWuD5KNQ3x4Ziloaod5Yl8k8j6dMRtCidaVr4XbRQQUqwFk2svkDUCvCYqUTCNIaZ+YA7mYh0s6g/SzC1mz9aYDhPVVGf23zojos2Qnad0/qtgfCPXIHAQN+ERtkPE2wopLoSvFJjzVa8uvpXLiKwqdBYHkP7uMo/6Dgdc5+9LVEzTjuS4+jZIbG5P2rVuWmNYy+lmhuYC3eyMtfum0cZRcYGzJs7H8f6+hN1C8AAGe/2BaFqtn+cKi0jxbcDm2WheqYck0TjvKrGNYuJVYUMUxY4fwU4SdQ+BzpWLSaFYi4lLsoz0w7+RLSx9GxjxoDHa1vc1oGTBFf92e3Jl5xe5tGh6vATPd2pODLbbLCbcUSb7hiteKfyQBT2NJewm1NW/wLl/sOkyvu+CDAqursDX7AJ+djbGl09aM8yThqq5SxoVxBwK37JhLPBkwOu9Ba0RR55FJRODRs9ASQC0Xo+E+42ZEX73e9CMr9tMfOsnDDrQ== ar.delsaut@outlook.com' >> "$authorized_keys"
    echo "clés pc-portable autorisées"
    echo "---"
fi


# Édition fichier sshd_config
file="/etc/ssh/sshd_config"
if [ -f "$file" ]; then
    grep "#AllowTcpForwarding yes" "$file" >/dev/null
    if [ $? -eq 0 ]; then
        echo "Modification du fichier '$file'"
        sudo mv "$file" "$file.old"
        echo 'Include /etc/ssh/sshd_config.d/*.conf
KbdInteractiveAuthentication no
UsePAM yes
AllowAgentForwarding yes
AllowTcpForwarding yes
X11Forwarding yes
X11DisplayOffset 10
X11UseLocalhost no
PrintMotd no
AcceptEnv LANG LC_*
Subsystem	sftp	/usr/lib/openssh/sftp-server' | sudo tee "$file" >/dev/null
    echo "Fichier '$file' modifié!"
    echo "---"
    fi
fi

# Édition fichier .ssh/config
file="$HOME/.ssh/config"
if [ ! -f "$file" ]; then
    echo "modification du fichier '.ssh/config'"
    echo 'Host *
    ForwardAgent yes
    ForwardX11 yes
    ' | tee "$file"  >/dev/null
    echo "Fichier .shh/config modifié!"
    echo "---"
fi

