#!/bin/bash

product_name=$(sudo dmidecode -s system-product-name)

if [ "$product_name" = "VMware Virtual Platform" ]; then
    sudo apt-get install -y open-vm-tools
fi

if [ "$product_name" = "VirtualBox" ]; then
    ancien_path="$PWD"
    sudo apt install "fasttrack-archive-keyring" -y
    line="# Dépot 1 FastTrack pour Virtualbox
    deb https://fasttrack.debian.net/debian-fasttrack/ bookworm-fasttrack main contrib"
    if ! grep -qxF "$line" /etc/apt/sources.list; then
        echo "$line" | sudo tee -a /etc/apt/sources.list >/dev/null
    fi
    line="# Dépot 1 FastTrack pour Virtualbox
    deb https://fasttrack.debian.net/debian-fasttrack/ bookworm-backports-staging main contrib"
    if ! grep -qxF "$line" /etc/apt/sources.list; then
        echo "$line" | sudo tee -a /etc/apt/sources.list >/dev/null
    fi
    sudo apt update
    sudo apt install virtualbox-guest-additions-iso --no-install-recommends -y
    # sudo apt install virtualbox-guest-x11 --no-install-recommends -y
    sudo apt install virtualbox-guest-utils --no-install-recommends -y
    sudo apt install build-essential dkms linux-headers-$(uname -r)  --no-install-recommends -y
    sudo mkdir -p /mnt/iso
    sudo mount -o loop "/usr/share/virtualbox/VBoxGuestAdditions.iso" /mnt/iso 2>/dev/null
    cd /mnt/iso
    sudo ./VBoxLinuxAdditions.run
    cd $ancien_path
    sudo umount /mnt/iso 
    sudo apt remove virtualbox-guest-additions-iso -y
    # sudo reboot
fi

