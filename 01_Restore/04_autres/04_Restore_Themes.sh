#!/bin/bash

clear

# Installation du curseur de theme
sudo apt-get install -y "papirus-icon-theme" --no-install-recommends 
sudo apt-get install -y "bibata-cursor-theme" --no-install-recommends 
sudo update-alternatives --install /usr/bin/x-cursor-theme x-cursor-theme /usr/share/icons/Bibata-Modern-Classic/cursor.theme 95
sudo update-alternatives --auto x-cursor-theme
lines="[Icon Theme]\nInherits=Bibata-Modern-Classic"
file="/usr/share/icons/default/index.theme"
if [ ! -f "$file" ]; then
    sudo touch "$file"
    sudo bash -c "echo -e '$lines' >> '$file'"
fi

