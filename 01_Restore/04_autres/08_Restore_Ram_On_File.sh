#!/bin/bash

clear

# Creation de swap (8gb), un fichier a la racine du systeme
sudo fallocate -l 8G /swapfile >/dev/null
sudo chmod 600 /swapfile >/dev/null
sudo mkswap /swapfile >/dev/null
sudo swapon /swapfile >/dev/null
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab >/dev/null
sudo sysctl vm.swappiness=10 >/dev/null
echo 'vm.swappiness=10' | sudo tee -a /etc/sysctl.conf >/dev/null
sudo sysctl vm.vfs_cache_pressure=50 >/dev/null
echo 'vm.vfs_cache_pressure=50' | sudo tee -a /etc/sysctl.conf >/dev/null

