#!/bin/bash

clear

# Audio
echo '
pipewire
wireplumber
pipewire-alsa
pipewire-jack
pipewire-pulse
pulseaudio-utils
#pulseaudio-module-raop
#pulseaudio-module-zeroconf
alsa-firmware-loaders
pavucontrol
libspa-0.2-modules
' >> "/tmp/packagelist_NonoOS.txt"

sed -i 's/#.*//' /tmp/packagelist_NonoOS.txt
sed -i '/^[[:space:]]*$/d' /tmp/packagelist_NonoOS.txt
chmod +x /tmp/packagelist_NonoOS.txt
sudo xargs apt install -y --no-install-recommends < /tmp/packagelist_NonoOS.txt
rm -rf /tmp/packagelist_NonoOS.txt

# LANG=C pactl info | grep '^Server Name'

if [ -e "$HOME/.local/state/wireplumber" ]; then
    rm -rf "$HOME/.local/state/wireplumber"
fi

# Pulseaudio
file="/etc/pulse/default.pa"
line="load-module module-raop-discover"
if [ -f "$file" ]; then
    if ! grep -Fxq "$line" "$file"; then
        echo "$line" | sudo tee -a "$file" >/dev/null
    fi
fi

pactl load-module module-raop-discover

# sudo apt install paprefs -y --no-install-recommends
# sudo ln -s /usr/lib/pulse-16.1+dfsg1 /usr/lib/pulse-16.1

