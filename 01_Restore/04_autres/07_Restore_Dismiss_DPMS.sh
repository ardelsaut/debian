#!/bin/bash

clear

file_path="/etc/X11/xorg.conf.d/serverflags.conf"
if [ ! -f "$file_path" ]; then
    sudo mkdir -p "/etc/X11/xorg.conf.d"
    content='Section "ServerFlags"
#...
Option "BlankTime" "0"
Option "StandbyTime" "0"
Option "SuspendTime" "0"
Option "OffTime" "0"
EndSection'
    echo "$content" | sudo tee "$file_path" >/dev/null
fi

