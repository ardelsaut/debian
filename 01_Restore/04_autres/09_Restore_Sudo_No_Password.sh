#!/bin/bash

clear

PACKAGE="sudo"
if ! apt -qq list $PACKAGE --installed 2>/dev/null | grep -q "$PACKAGE"; then
    sudo apt install $PACKAGE  -y --no-install-recommends 
fi



old_string="ALL=(ALL:ALL) ALL"
new_string="ALL=(ALL) NOPASSWD: ALL"

sudo sed -i "/^%sudo/ s/$old_string/$new_string/" "/etc/sudoers"

