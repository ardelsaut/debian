#!/bin/bash

# Xfce4
if [[ "$XDG_CURRENT_DESKTOP" == *"XFCE"* ]]; then
    WALLPAPER_PATH="/usr/share/pixmaps/Debian_ascii_1920.png"
    if [ ! -e "$WALLPAPER_PATH" ]; then
        sudo cp -rf "$HOME/Images/Wallpapers/Debian_ascii_1920.png" "/usr/share/pixmaps"
    fi
    xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitoreDP-1/workspace0/last-image" -s "$WALLPAPER_PATH"
fi

# Cinnamon
if [[ "$XDG_CURRENT_DESKTOP" == *"cinnamon"* ]]; then
    if [ ! -e "$WALLPAPER_PATH" ]; then
        sudo cp -rf "$HOME/Images/Wallpapers/Debian_ascii_1920.png" "/usr/share/pixmaps"
    fi
    gsettings set org.cinnamon.desktop.background picture-uri "file://$WALLPAPER_PATH"
fi

