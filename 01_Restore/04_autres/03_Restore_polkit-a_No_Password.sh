#!/bin/bash

clear

sudo apt install policykit-1 policykit-1-gnome polkitd -y --no-install-recommends 

file_path="/etc/polkit-1/rules.d/49-nopasswd_limited.rules"

content='/* Allow members of the wheel group to execute any actions
 * without password authentication, similar to "sudo NOPASSWD:"
 */
polkit.addRule(function(action, subject) {
    if (subject.isInGroup("sudo")) {
        return polkit.Result.YES;
    }
});'


if [ ! -f "$file_path" ]; then
    echo "$content" | sudo tee "$file_path" >/dev/null
else
    mv "$file_path" "$file_path.old"
    echo "$content" | sudo tee "$file_path" >/dev/null
fi 

