#!/bin/bash

clear

PACKAGE="gdm3"
if ! apt -qq list $PACKAGE --installed 2>/dev/null | grep -q "$PACKAGE"; then
    sudo apt install $PACKAGE  -y --no-install-recommends 
fi

conf_file="/etc/gdm3/daemon.conf"
if [ -f "$conf_file" ]; then
    if grep -q "\[daemon\]" "$conf_file"; then
        sudo sed -i '/\[daemon\]/a AutomaticLoginEnable = true' "$conf_file"
        sudo sed -i "/AutomaticLoginEnable = true/a AutomaticLogin = $USER" "$conf_file"
    fi
fi

search_line="AutomaticLogin = $USER"
count=$(grep -c "$search_line" "$conf_file")
if [ $count -gt 1 ]; then
    sudo sed -i '0,/'"$search_line"'/!{/^'"$search_line"'/d;}' "$conf_file"
fi

search_line="AutomaticLoginEnable = true"
count=$(grep -c "$search_line" "$conf_file")
if [ $count -gt 1 ]; then
    sudo sed -i '0,/'"$search_line"'/!{/^'"$search_line"'/d;}' "$conf_file"
fi


# Restart GDM3 for changes to take effect
# systemctl restart gdm3

