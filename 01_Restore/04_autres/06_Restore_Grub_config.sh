#!/bin/bash

clear

if [ ! -e "/usr/share/pixmaps/Debian_ascii_1920.png" ]; then
    sudo cp -rf "$HOME/Images/Wallpapers/Debian_ascii_1920.png" "/usr/share/pixmaps"
fi

config_file="/etc/default/grub"
sudo sed -i 's/^GRUB_TIMEOUT=.*/GRUB_TIMEOUT=3/' "$config_file"
sudo sed -i 's/GRUB_DEFAULT=.*/GRUB_DEFAULT=saved/' "$config_file"
echo 'GRUB_SAVEDEFAULT=true' | sudo tee -a "$config_file"
echo "GRUB_BACKGROUND=/usr/share/pixmaps/Debian_ascii_1920.png" | sudo tee -a "$config_file"
sudo update-grub

