#!/bin/bash

clear

file="/usr/lib/udev/rules.d/90-brightnessctl.rules"


lines='ACTION=="add", SUBSYSTEM=="backlight", RUN+="bright-helper video g+w /sys/class/backlight/%k/brightness"
ACTION=="add", SUBSYSTEM=="leds",      RUN+="bright-helper input g+w /sys/class/leds/%k/brightness"'


if [ ! -f "$file" ]; then
    sudo touch "$file"
    sudo bash -c "echo -e '$lines' >> '$file'"
fi

