#!/bin/bash

clear

PACKAGE="network-manager"
if ! apt -qq list $PACKAGE --installed 2>/dev/null | grep -q "$PACKAGE"; then
    sudo apt install $PACKAGE  -y --no-install-recommends 
fi


sudo sed -i 's/^managed=false$/managed=true/' /etc/NetworkManager/NetworkManager.conf

# Pour préparer le wifi
# sudo sed -i '/# The primary network interface/,$d' /etc/network/interfaces
# Désactivation de default internet au profit de NetworkManager
sudo mv /etc/network/interfaces /etc/network/interfaces.old
content="# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
#allow-hotplug wlp0s20f3
#iface wlp0s20f3 inet dhcp
#       wpa-ssid Nono_5GHz
#       wpa-psk  bace090890
"
echo "$content" | sudo tee /etc/network/interfaces >/dev/null

# fichiers wifi
path_Gsm="/etc/NetworkManager/system-connections/Galaxy_S10e66de.nmconnection"
if [ ! -e "$path_Gsm" ]; then
    ssid_Gsm="Galaxy_S10e66de"
    password_Gsm="abcd1234"

    basename=$(basename "$path_Gsm")
    basename_no_ext="${basename%.*}"

    echo "[connection]
id=$basename_no_ext
uuid=d65f4629-64a4-4ef7-b5bd-4b05fe6823a0
type=wifi

[wifi]
mode=infrastructure
ssid=$ssid_Gsm

[wifi-security]
key-mgmt=wpa-psk
psk=$password_Gsm

[ipv4]
method=auto

[ipv6]
addr-gen-mode=default
method=auto

[proxy]
" |sudo tee -a "$path_Gsm"

    sudo chown root:root "$path_Gsm"
    sudo chmod 600 "$path_Gsm"
fi


path_Nono_5GHz="/etc/NetworkManager/system-connections/Nono_5GHz.nmconnection"
if [ ! -e "$path_Nono_5GHz" ]; then
ssid_Nono_5GHz="Nono_5GHz"
password_Nono_5GHz="bace090890"

basename=$(basename "$path_Nono_5GHz")
basename_no_ext="${basename%.*}"

echo "[connection]
id=$basename_no_ext
uuid=d65f4629-64a4-4ef7-b5bd-4b05fe6823a0
type=wifi

[wifi]
mode=infrastructure
ssid=$ssid_Nono_5GHz

[wifi-security]
key-mgmt=wpa-psk
psk=$password_Nono_5GHz

[ipv4]
method=auto

[ipv6]
addr-gen-mode=default
method=auto

[proxy]
" |sudo tee -a "$path_Nono_5GHz"

sudo chown root:root "$path_Nono_5GHz"
sudo chmod 600 "$path_Nono_5GHz"
fi

