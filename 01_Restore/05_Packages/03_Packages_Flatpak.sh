#!/bin/bash
if ! command -v flatpak &> /dev/null; then
    sudo apt update
    sudo apt install flatpak -y --no-install-recommends
    sudo apt install flatpak-xdg-utils -y --no-install-recommends
fi

sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
sudo flatpak install \
"md.obsidian.Obsidian" \
"com.ktechpit.whatsie" \
"com.discordapp.Discord" \
"io.github.shiftey.Desktop" \
"org.nomacs.ImageLounge" \
-y 

# "com.synology.SynologyDrive"

