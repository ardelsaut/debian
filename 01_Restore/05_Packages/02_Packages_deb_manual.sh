#!/bin/bash

#### VSCodium #### -----------------------------------------------------------------------------
sudo wget https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg -O /usr/share/keyrings/vscodium-archive-keyring.asc
echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.asc ] https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs vscodium main'     | sudo tee /etc/apt/sources.list.d/vscodium.list
sudo apt-get update
sudo apt-get install codium -y

#### Firefox #### -----------------------------------------------------------------------------
sudo install -d -m 0755 /etc/apt/keyrings
wget -q https://packages.mozilla.org/apt/repo-signing-key.gpg -O- | sudo tee /etc/apt/keyrings/packages.mozilla.org.asc > /dev/null
echo "deb [signed-by=/etc/apt/keyrings/packages.mozilla.org.asc] https://packages.mozilla.org/apt mozilla main" | sudo tee -a /etc/apt/sources.list.d/mozilla.list > /dev/null
echo '
Package: *
Pin: origin packages.mozilla.org
Pin-Priority: 1000
' | sudo tee /etc/apt/preferences.d/mozilla
sudo apt-get update && sudo apt-get install firefox -y

#### Parsec #### -----------------------------------------------------------------------------
LIBJPEG8_PACKAGE_URL="https://archive.debian.org/debian/pool/main/libj/libjpeg8/libjpeg8_8b-1_amd64.deb"
LIBJPEG8_PACKAGE_FILENAME="/tmp/libjpeg8_8b-1_amd64.deb"
wget "$LIBJPEG8_PACKAGE_URL" -O "$LIBJPEG8_PACKAGE_FILENAME"
sudo apt-get install "$LIBJPEG8_PACKAGE_FILENAME" -y
rm "$LIBJPEG8_PACKAGE_FILENAME"
PARSEC_PACKAGE_URL="https://builds.parsec.app/package/parsec-linux.deb"
PARSEC_PACKAGE_FILENAME="/tmp/parsec-linux.deb"
wget "$PARSEC_PACKAGE_URL" -O "$PARSEC_PACKAGE_FILENAME"
sudo apt-get install "$PARSEC_PACKAGE_FILENAME" -y
rm "$PARSEC_PACKAGE_FILENAME"

#### Touchegg #### -----------------------------------------------------------------------------
owner="JoseExposito"
repo="touchegg"
latest_release=$(wget -qO- "https://api.github.com/repos/$owner/$repo/releases/latest")
url=$(echo "$latest_release" | grep -oP '"browser_download_url": "\K([^"]+\.deb)' | head -n 1)
if [ -n "$url" ]; then
    wget "$url" -P /tmp
    sudo apt-get install -y "/tmp/$(basename "$url")"
    rm -f "/tmp/$(basename "$url")"
fi

#### Synology Drive #### -----------------------------------------------------------------------------
URL="https://archive.synology.com/download/Utility/SynologyDriveClient/"
HTML_CONTENT=$(curl -s "$URL")
LATEST_VERSION=$(echo "$HTML_CONTENT" | grep -oE 'SynologyDriveClient/[0-9]+\.[0-9]+\.[0-9]+-[0-9]+' | head -n1)
VERSION_NUMBER=${LATEST_VERSION##*-}
wget "https://global.synologydownload.com/download/Utility/$LATEST_VERSION/Ubuntu/Installer/synology-drive-client-$VERSION_NUMBER.x86_64.deb" -P /tmp
sudo apt-get install -y "/tmp/synology-drive-client-$VERSION_NUMBER.x86_64.deb"
rm -f "/tmp/synology-drive-client-$VERSION_NUMBER.x86_64.deb"

#### Steam #### -----------------------------------------------------------------------------
sudo dpkg --add-architecture i386
sudo apt update
sudo apt install \
"steam-libs-i386" \
"steam-installer" \
"steam-libs" \
-y \
--no-install-recommends

