#!/bin/bash

xdg-mime default pcmanfm.desktop inode/directory

#  Update Fonts
sudo fc-cache -f -v

# Nettoyage de apt
sudo apt autoclean -y
sudo apt autoremove -y

# Redémarrage
# sudo reboot

