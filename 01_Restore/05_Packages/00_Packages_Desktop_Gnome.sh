#!/bin/bash


# Fixe erreur lors de 'apt install' après avoir delet $HOME
cd /
cd "$HOME"

echo '
gnome-core
gnome-tweaks
gnome-software-plugin-flatpak
fonts-dejavu
network-manager-gnome
gnome-shell-extensions
gnome-shell-extension-appindicator
gnome-shell-extension-hide-activities
#gnome-shell-extension-arc-menu
gnome-shell-extension-autohidetopbar
#gnome-shell-extension-bluetooth-quick-connect
gnome-shell-extension-caffeine
gnome-shell-extension-manager
gnome-shell-extension-no-annoyance
#gnome-shell-extension-dashtodock
#gnome-shell-extension-dash-to-panel
gnome-shell-extension-gpaste                   
gnome-shell-extension-tiling-assistant
gnome-shell-extension-gsconnect
gnome-shell-extension-gsconnect-browsers
#gnome-shell-extension-weather
nautilus-admin
' >> /tmp/packagelist_NonoOS.txt

sed -i 's/#.*//' /tmp/packagelist_NonoOS.txt
sed -i '/^[[:space:]]*$/d' /tmp/packagelist_NonoOS.txt
chmod +x /tmp/packagelist_NonoOS.txt
sudo xargs apt install -y --no-install-recommends < /tmp/packagelist_NonoOS.txt
rm -rf /tmp/packagelist_NonoOS.txt

