#!/bin/bash

# set -e

# Fixe erreur lors de 'apt install' après avoir delet $HOME
cd /
cd "$HOME"



# # Pulseaudio
# pulseaudio
# pulseaudio-module-raop
# pulseaudio-module-zeroconf
# pcmanfm


# Video
echo '
xserver-xorg
xserver-xorg-core
xserver-xorg-video-intel
xserver-xorg-input-libinput
x11-apps
x11-utils
xarchiver
xdg-desktop-portal-gtk
xdo
xdotool
xinput
' >> "/tmp/packagelist_NonoOS.txt"


# Applications
echo '
fzf
htop
inkscape
git
guake
micro
xclip
mpv
thunderbird
# thunderbird-gtk3
thunderbird-l10n-fr
' >> "/tmp/packagelist_NonoOS.txt"


# Theme
echo '
adwaita-qt
adwaita-qt6
papirus-icon-theme
bibata-cursor-theme
gnome-themes-extra-data
qt5ct
' >> "/tmp/packagelist_NonoOS.txt"


# Fonts
echo '
fonts-cantarell
fonts-dejavu-core
fonts-font-awesome
ttf-mscorefonts-installer
ttf-xfree86-nonfree
' >> "/tmp/packagelist_NonoOS.txt"


# Langues
echo '
hunspell-fr
hunspell-fr-classical
task-french
task-french-desktop
' >> "/tmp/packagelist_NonoOS.txt"


# Firmware
echo '
firmware-b43-installer
firmware-b43legacy-installer
firmware-linux
firmware-linux-nonfree
' >> "/tmp/packagelist_NonoOS.txt"


# Systèmes
echo '
accountsservice
acpi
bash
bash-completion
bat
brightnessctl
cifs-utils
curl
dbus
dbus-bin
dbus-daemon
dbus-session-bus-common
dbus-system-bus-common
dbus-user-session
debconf-utils
fd-find
ffmpeg
gnome-keyring
gstreamer1.0-plugins-bad
gstreamer1.0-plugins-ugly
gtk2-engines-murrine
gvfs
gvfs-backends
gvfs-fuse
haveged
lsb-release
network-manager
nmap
ntfs-3g
pigz
pkexec
rofi
tumbler
user-setup
wget
whois
wpasupplicant
' >> "/tmp/packagelist_NonoOS.txt"


# Bluetooth
echo '
blueman
bluetooth
bluez
bluez-cups
bluez-firmware
bluez-obexd
libspa-0.2-bluetooth
' >> "/tmp/packagelist_NonoOS.txt"


sed -i 's/#.*//' /tmp/packagelist_NonoOS.txt
sed -i '/^[[:space:]]*$/d' /tmp/packagelist_NonoOS.txt
chmod +x /tmp/packagelist_NonoOS.txt
sudo xargs apt install -y --no-install-recommends < /tmp/packagelist_NonoOS.txt
rm -rf /tmp/packagelist_NonoOS.txt


# Pour lire dvd, mais demande interaction utilisateur, a fixer
#libdvd-pkg


# fonts-powerline
# powerline-daemon -q
# POWERLINE_BASH_CONTINUATION=1
# POWERLINE_BASH_SELECT=1
# source /usr/share/powerline/bindings/bash/powerline.sh

