#!/bin/bash

clear

dossierBackup="/mnt/nas/archives/PC/Linux/Backup_Home"



while true; do
    clear
    echo "en attente que le nas soit disponnible et bien monté"
    clear
    if [ "$(find "$dossierBackup" -mindepth 1 -print -quit)" ]; then
        echo "Dossier nas accessible!"
        break
    fi
    sleep 0.1
done

sudo rm -rf $HOME

latest_tar=$(ls -lt /mnt/nas/archives/PC/Linux/Backup_Home/*.tar | awk '{print $NF}' | head -n 1)

sudo tar -xvf "$latest_tar" -C /

if [ -e "$HOME/.local/state/wireplumber" ]; then
    rm -rf "$HOME/.local/state/wireplumber"
fi

sudo rm -rf $HOME/.cache

sudo rm -rf "/home/nono/.var/app/com.ktechpit.whatsie"

cd /
cd $HOME

# Création des dossiers utilisateur de base
# xdg-user-dirs-update

