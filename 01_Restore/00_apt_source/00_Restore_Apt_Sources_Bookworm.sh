#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
export DEBIAN_PRIORITY=critical
echo '* libraries/restart-without-asking boolean true' | sudo debconf-set-selections

sudo apt install "ca-certificates" -y --no-install-recommends
sudo apt install "systemd-timesyncd" -y --no-install-recommends

fichierSourcesApt="/etc/apt/sources.list"

sudo rm "$fichierSourcesApt"

content='# Fichier sources apt par Nono
#deb http://deb.debian.org/debian/ bookworm main contrib non-free non-free-firmware
deb http://ftp-stud.hs-esslingen.de/debian/ bookworm main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian bookworm main contrib non-free non-free-firmware

# deb http://deb.debian.org/debian-security/ bookworm-security main contrib non-free non-free-firmware
deb http://security.debian.org/debian-security bookworm-security main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian-security/ bookworm-security main contrib non-free non-free-firmware



#deb http://deb.debian.org/debian/ bookworm-updates main contrib non-free non-free-firmware
deb http://ftp-stud.hs-esslingen.de/debian/ bookworm-updates main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian bookworm-updates main contrib non-free non-free-firmware

#deb http://deb.debian.org/debian/ bookworm-backports main contrib non-free non-free-firmware
deb http://ftp-stud.hs-esslingen.de/debian/ bookworm-backports main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian bookworm-backports main contrib non-free non-free-firmware'

echo "$content" | sudo tee $fichierSourcesApt > /dev/null

sudo apt update
sudo apt-get dist-upgrade --yes
echo '* libraries/restart-without-asking boolean false' | sudo debconf-set-selections

sudo apt autoremove -y

