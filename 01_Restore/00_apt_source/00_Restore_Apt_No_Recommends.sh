#!/bin/bash

FILE="/etc/apt/apt.conf.d/00pas-de-recommends"
LINE1='APT::Install-Recommends "false";'
LINE2='APT::Install-Suggests "false";'

if [[ ! -f "$FILE" ]]; then
    sudo touch "$FILE"
fi

if ! grep -qF "$LINE" "$FILE"; then
    echo "$LINE1"  | sudo tee -a $FILE >/dev/null
fi

if ! grep -qF "$LINE" "$FILE"; then
    echo "$LINE2"  | sudo tee -a $FILE >/dev/null
fi

