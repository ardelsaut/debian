#!/bin/bash


# Fixe erreur lors de 'apt install' après avoir delet $HOME
cd /
cd "$HOME"

echo '
bspwm
dunst
feh
i3lock-fancy
lightdm
numlockx
polybar
spacefm
sxhkd
xautolock
dkms
libinput-tools
picom
kitty
' >> /tmp/packagelist_NonoOS.txt

sed -i 's/#.*//' /tmp/packagelist_NonoOS.txt
sed -i '/^[[:space:]]*$/d' /tmp/packagelist_NonoOS.txt
chmod +x /tmp/packagelist_NonoOS.txt
sudo xargs apt install -y --no-install-recommends < /tmp/packagelist_NonoOS.txt
rm -rf /tmp/packagelist_NonoOS.txt

