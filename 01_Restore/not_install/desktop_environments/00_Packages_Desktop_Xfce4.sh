#!/bin/bash


# Fixe erreur lors de 'apt install' après avoir delet $HOME
cd /
cd "$HOME"

echo '
lightdm
xfce4
xfce4-notifyd
xfce4-power-manager-plugins
xfce4-terminal
xfce4-whiskermenu-plugin
mugshot
librsvg2-common 
' >> /tmp/packagelist_NonoOS.txt

sed -i 's/#.*//' /tmp/packagelist_NonoOS.txt
sed -i '/^[[:space:]]*$/d' /tmp/packagelist_NonoOS.txt
chmod +x /tmp/packagelist_NonoOS.txt
sudo xargs apt install -y --no-install-recommends < /tmp/packagelist_NonoOS.txt
rm -rf /tmp/packagelist_NonoOS.txt

