#!/bin/bash

# export NEEDRESTART_MODE=a
export DEBIAN_FRONTEND=noninteractive
## Questions that you really, really need to see (or else). ##
export DEBIAN_PRIORITY=critical
echo '* libraries/restart-without-asking boolean true' | sudo debconf-set-selections

# Mise à jour de la liste de sources debian:
# sudo apt update && sudo apt upgrade -y

# sudo apt full-upgrade -y

sudo apt install "ca-certificates" -y --no-install-recommends
sudo apt install "systemd-timesyncd" -y --no-install-recommends

fichierSourcesApt="/etc/apt/sources.list"

sudo rm "$fichierSourcesApt"

content='# Fichier sources apt par Nono
#deb http://deb.debian.org/debian/ trixie main contrib non-free non-free-firmware
deb http://ftp-stud.hs-esslingen.de/debian/ trixie main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian trixie main contrib non-free non-free-firmware

# deb http://deb.debian.org/debian-security/ trixie-security main contrib non-free non-free-firmware
deb http://security.debian.org/debian-security trixie-security main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian-security/ trixie-security main contrib non-free non-free-firmware



#deb http://deb.debian.org/debian/ trixie-updates main contrib non-free non-free-firmware
deb http://ftp-stud.hs-esslingen.de/debian/ trixie-updates main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian trixie-updates main contrib non-free non-free-firmware

#deb http://deb.debian.org/debian/ trixie-backports main contrib non-free non-free-firmware
deb http://ftp-stud.hs-esslingen.de/debian/ trixie-backports main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian trixie-backports main contrib non-free non-free-firmware'

echo "$content" | sudo tee $fichierSourcesApt > /dev/null


sudo apt-get update
sudo apt-get dist-upgrade --yes
echo '* libraries/restart-without-asking boolean false' | sudo debconf-set-selections

sudo apt autoremove

# deb https://deb.debian.org/debian/ testing contrib main non-free non-free-firmware
# deb https://deb.debian.org/debian/ testing-updates contrib main non-free non-free-firmware
# deb https://deb.debian.org/debian/ testing-proposed-updates contrib main non-free non-free-firmware
# deb https://deb.debian.org/debian/ testing-backports contrib main non-free non-free-firmware
# deb https://security.debian.org/debian-security/ testing-security contrib main non-free non-free-firmware'

