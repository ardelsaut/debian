#!/bin/bash

# export NEEDRESTART_MODE=a
export DEBIAN_FRONTEND=noninteractive
## Questions that you really, really need to see (or else). ##
export DEBIAN_PRIORITY=critical
echo '* libraries/restart-without-asking boolean true' | sudo debconf-set-selections

# Mise à jour de la liste de sources debian:
# sudo apt update && sudo apt upgrade -y

# sudo apt full-upgrade -y

sudo apt install "ca-certificates" -y --no-install-recommends
sudo apt install "systemd-timesyncd" -y --no-install-recommends

fichierSourcesApt="/etc/apt/sources.list"

sudo rm "$fichierSourcesApt"

content='# Fichier sources apt par Nono
deb https://ftp.debian.org/debian/ sid contrib main non-free non-free-firmware'

# content='# Fichier sources apt par Nono
# deb http://deb.debian.org/debian unstable main
# deb http://deb.debian.org/debian-debug unstable-debug main
# deb http://deb.debian.org/debian-ports unstable main'

echo "$content" | sudo tee $fichierSourcesApt > /dev/null

sudo apt-get update
sudo apt-get dist-upgrade --yes
echo '* libraries/restart-without-asking boolean false' | sudo debconf-set-selections

sudo apt autoremove