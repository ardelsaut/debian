#!/bin/bash

clear

sudo apt install  lightdm -y --no-install-recommends

if [ ! -e "/usr/share/pixmaps/Debian_ascii_1920.png" ]; then
    sudo cp -rf "$HOME/Images/Wallpapers/Debian_ascii_1920.png" "/usr/share/pixmaps"
fi

if [ ! -e "/usr/share/pixmaps/avatar_icone.ico" ]; then
    sudo cp -rf "$HOME/Images/icons/avatar_icone.ico" "/usr/share/pixmaps"
fi

# Edition de /etc/lightdm/lightdm.conf
    # AutoLogin
sudo sed -i "/^#*autologin-user=/c\autologin-user=$USER" /etc/lightdm/lightdm.conf
sudo sed -i "/^#*autologin-user-timeout=/c\autologin-user-timeout=0" /etc/lightdm/lightdm.conf
    # Voir noms utilisateurs
sudo sed -i "/^#*greeter-hide-users=false/c\greeter-hide-users=false" /etc/lightdm/lightdm.conf
    # Activer numlock
sudo sed -i "/^#*greeter-setup-script=/c\greeter-setup-script=/usr/bin/numlockx on" /etc/lightdm/lightdm.conf

#  Édition de /etc/lightdm/lightdm-gtk-greeter.conf 
file="/etc/lightdm/lightdm-gtk-greeter.conf"
if ! grep -qF "theme-name=Adwaita-dark" "$file"; then
    echo "theme-name=Adwaita-dark" | sudo tee -a "$file" >/dev/null
    echo "indicators= ~spacer;~clock;~spacer;~session;~power" | sudo tee -a "$file" >/dev/null
    echo "clock-format= %H:%M:%S" | sudo tee -a "$file" >/dev/null
    echo "background = /usr/share/pixmaps/Debian_ascii_1920.png" | sudo tee -a "$file" >/dev/null
    echo "icon-theme-name = Papirus-Dark" | sudo tee -a "$file" >/dev/null
    echo "cursor-theme-name = Bibata-Modern-Classic" | sudo tee -a "$file" >/dev/null
    echo "position = 75%,start 50%,center" | sudo tee -a "$file" >/dev/null
    echo "default-user-image = /usr/share/pixmaps/avatar_icone.ico" | sudo tee -a "$file" >/dev/null
    echo "xft-antialias= true" | sudo tee -a "$file" >/dev/null
    echo "xft-dpi= 96" | sudo tee -a "$file" >/dev/null
    echo "xft-hintstyle= hintslight" | sudo tee -a "$file" >/dev/null
    echo "xft-rgba= rgb" | sudo tee -a "$file" >/dev/null
    echo "font-name= Noto Mono 16" | sudo tee -a "$file" >/dev/null
fi
#sudo systemctl restart lightdm

