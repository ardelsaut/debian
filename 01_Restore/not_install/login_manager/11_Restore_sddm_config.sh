#!/bin/bash
# SDDM config



clear

sudo apt install sddm -y --no-install-recommends 

# Autologin sur xfce4
sudo mv /etc/sddm.conf /etc/sddm.conf.old
echo "[Autologin]
User=$USER
Session=xfce.desktop
Relogin=false
" | sudo tee /etc/sddm.conf


# Session=xfce.desktop
# sudo mkdir -p /usr/share/sddm/themes/nonoos