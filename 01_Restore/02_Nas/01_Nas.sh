#!/bin/bash

clear

    # Titre
echo "Connexion au Nas:"
echo "---"

    # Vérification des packages .deb
dpkg -l | grep "cifs-utils" >/dev/null
if [ $? -eq 1 ]; then
    echo "Paquet 'cifs-utils' non installé"
    echo "Installation du paquet 'cifs-utils'"
    echo "Mise à jour des sources apt en cours ..."
    sudo apt-get update > /dev/null 2>&1
    echo "Installation du paquet 'cifs-utils' en cours..."
    sudo apt-get install cifs-utils -y --no-install-recommends > /dev/null 2>&1
    echo "Paquet 'cifs-utils' installé!"
    echo "---"
fi

    # Création du dossier credential
dir_NonoOS_Nas="$HOME/.config/NonoOS/Nas"
if [ ! -d "$dir_NonoOS_Nas" ]; then
    echo "Création du dossier '$dir_NonoOS_Nas'."
    sudo mkdir -p "$dir_NonoOS_Nas"  
    echo "Dossier credential créé!"
    echo "---"
fi

    # Création du fichier credential
fichierCredential="$dir_NonoOS_Nas/nas_credentials"
if [ ! -f "$fichierCredential" ]; then
    echo "Création du fichier credentials $fichierCredential..."
    echo "username=Nono" | sudo tee "$fichierCredential"
    echo "password=Poul€44211030" | sudo tee -a "$fichierCredential"
    echo "Fichier credential créé!"
    echo "---"
fi


paths="/mnt/nas/archives /mnt/nas/downloads /mnt/nas/drive /mnt/nas/video"
for path in $paths
    do
    file_name=$(basename "$path")
    file_name_no_ext="${file_name%.*}"
    file_path="/etc/systemd/system/mount-nas-$file_name_no_ext.service"
    if [ ! -d "$path" ]; then
        echo "Création du dossier de montage pour: $path"
        echo "---"
        sudo mkdir -p "$path"
        echo "Dossier $path: OK"
        echo "---"
    fi

    if [ ! -f "$file_path" ]; then
        echo "Création du fichier service: $file_path"
        echo "---"
        sudo tee "$file_path" > /dev/null <<EOF
[Unit]
Description=Mount Nas-$file_name_no_ext at startup
After=network-online.target
Wants=network-online.target

[Service]
Type=oneshot
ExecStartPre=/bin/sleep 5
ExecStart=/bin/mount -t cifs //nonobouli.myds.me/$file_name_no_ext $path -o credentials=$fichierCredential,uid=$(id -u),gid=$(id -g),file_mode=0644,dir_mode=0755,vers=3.0

[Install]
WantedBy=multi-user.target
EOF
        echo "Fichier $file_path: OK"
        echo "---"
    fi

    if ! systemctl is-enabled "mount-nas-$file_name_no_ext.service" &> /dev/null; then
        echo "activation du service mount-nas-$file_name_no_ext.service"
        echo "---"
        sudo systemctl enable "mount-nas-$file_name_no_ext.service"
        echo "---"
    fi
done

sudo systemctl daemon-reload
if [ -z "$(ls -A /mnt/nas/archives/)" ] || systemctl is-active mount-nas-archives.service | grep -q "failed"; then
    echo "Directory is empty"
    sudo umount /mnt/nas/archives/ &> /dev/null
    sudo systemctl restart --now "mount-nas-archives.service" &
fi

if [ -z "$(ls -A /mnt/nas/drive/)" ] || systemctl is-active mount-nas-drive.service | grep -q "failed"; then
    echo "Directory is empty"
    sudo umount /mnt/nas/drive/ &> /dev/null
    sudo systemctl restart --now "mount-nas-drive.service" &
fi

if [ -z "$(ls -A /mnt/nas/video/)" ] || systemctl is-active mount-nas-video.service | grep -q "failed"; then
    echo "Directory is empty"
    sudo umount /mnt/nas/video/ &> /dev/null
    sudo systemctl restart --now "mount-nas-video.service" &
fi

if [ -z "$(ls -A /mnt/nas/downloads/)" ] || systemctl is-active mount-nas-downloads.service | grep -q "failed"; then
    echo "Directory is empty"
    sudo umount /mnt/nas/downloads/ &> /dev/null
    sudo systemctl restart --now "mount-nas-downloads.service" &
fi

echo "Dossiers Nas montés"
echo "---"

