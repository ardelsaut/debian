#!/bin/bash

# Input file
input_file="/home/nono/history.log"

# Temporary file
temp_file=$(mktemp)

# Loop through each line in the input file
while IFS= read -r line; do
    # Check if the line starts with "Commandline:"
    if [[ $line == Commandline:* ]]; then
        # If yes, write the line to the temporary file
        echo "$line" >> "$temp_file"
    fi
done < "$input_file"

# Replace the original file with the temporary file
mv "$temp_file" "$input_file"
touch "/home/nono/Desktop/history.log"

echo "Lines starting with 'Commandline:' have been preserved."
