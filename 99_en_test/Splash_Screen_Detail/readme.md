# Change Splashscreen to deatail console command

- Lancer la commande: 

```bash

sudo plymouth-set-default-theme -R details

```

- si erreur sur disque encrypted (lvm):
    
    - créer un fichier (s'il n'existe pas): 

```bash      

sudo nano /etc/initramfs-tools/conf.d/resume        

```
- Introduire :

```bash
        
RESUME=none

```
- Sauvegarder le fichier.

- être sûr que 'cryptsetup-initramfs'

```bash
sudo apt-get install cryptsetup-initramfs est installé
```

- être sûr que '/etc/plymouth/plymouthd.conf' contient:

```

[Daemon]
Theme=details

```

```bash

cat /etc/plymouth/plymouthd.conf

```

- et relancer la commande :

```bash

    sudo plymouth-set-default-theme -R details

```
- Redémarrer

```bash

sudo reboot

```