# Permettre la lecture Airplay sur pipewiore

- Éditer le fichier:

```bash

sudo nano /etc/pulse/default.pa

```

- ajouter : 

```

load-module module-raop-discover

```

- !!! A faire si seulement ça ne marche psa


```bash

# Pas sûr que cela soit nécessaire. A essayer d'abord sans!
sudo apt install libpipewire-0.3-0

```

- Creer archive '~/.config/pipewire/pipewire.conf.d/raop-discover.conf'

```bash

nano ~/.config/pipewire/pipewire.conf.d/raop-discover.conf

```

- ajouter : 

```bash

context.modules = [
    {
        name = libpipewire-module-raop-discover
        args = { }
    }
]

```

- Charger le module :

```bash

pactl load-module module-raop-discover
# si erreur, essayer : 
# pactl load-module libpipewire-module-raop-discover

```

- Relancer les services :

```bash

systemctl enable --now avahi-daemon.service
systemctl --user restart pipewire-pulse.service 

```


- !!! Only if needed:

   - replace pulseaudio if needed

   - see :    https://wiki.debian.org/PipeWire

```bash

# Liste des indispensables :
sudo apt install pipewire-pulse libspa-0.2-bluetooth libspa-0.2-jack wireplumber pipewire-jack pipewire-alsa

```
   