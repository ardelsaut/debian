#!/bin/bash

# Function to retrieve the active WiFi interface
get_wifi_interface() {
    # Use the ip command to list the active network interfaces
    # Filter out loopback, non-physical, and non-wireless interfaces
    # Extract the first non-loopback, non-virtual wireless interface
    interface=$(ip -o link show | awk -F': ' '{print $2}' | grep -v '^lo' | grep -v '@' | grep -i 'wl' | head -n 1)
    echo "$interface"
}

# Function to calculate traffic rate in KB/s
calculate_rate() {
    current=$1
    previous=$2
    time_diff=$3

    # Ensure time difference is greater than 0 to avoid division by zero
    if [ $time_diff -gt 0 ]; then
        rate=$(( ($current - $previous) / $time_diff ))
        echo "$rate"
    else
        echo "0"
    fi
}

# Function to convert KB/s to MB/s if greater than 1000 KB/s
convert_to_MBps() {
    rate=$1
    if [ -n "$rate" ] && [ "$rate" -gt 1000 ]; then
        rate=$(echo "scale=2; $rate / 1024" | bc)
        echo "$rate MB/s"
    else
        echo "$rate KB/s"
    fi
}

# Main loop
while true; do
    # Retrieve the WiFi interface
    wifi_interface=$(get_wifi_interface)

    # Check if a WiFi interface is found
    if [ -n "$wifi_interface" ]; then
        # Read the contents of /proc/net/dev and filter the line containing the WiFi interface
        value=$(cat /proc/net/dev | grep "$wifi_interface:")

        # Extract incoming and outgoing traffic values
        current_incoming=$(echo "$value" | awk '{print $2}')
        current_outgoing=$(echo "$value" | awk '{print $10}')

        # Calculate time difference (in seconds)
        current_time=$(date +%s)
        if [ -n "$previous_time" ]; then
            time_diff=$((current_time - previous_time))
        else
            time_diff=1  # Default to 1 second to avoid division by zero
        fi

        # Calculate incoming and outgoing traffic rates in KB/s
        incoming_rate=$(calculate_rate $current_incoming $previous_incoming $time_diff)
        outgoing_rate=$(calculate_rate $current_outgoing $previous_outgoing $time_diff)

        # Convert rates to MB/s if greater than 1000 KB/s
        incoming_rate=$(convert_to_MBps $incoming_rate)
        outgoing_rate=$(convert_to_MBps $outgoing_rate)

        # Print the values
		clear
        echo -n "$incoming_rate"
        echo -n "   $outgoing_rate"

        # Store current values for the next iteration
        previous_incoming=$current_incoming
        previous_outgoing=$current_outgoing
        previous_time=$current_time
    else
        echo "No active WiFi interface found."
    fi

    # Adjust the sleep interval as needed
    sleep 1  # This will calculate rates per second
done
