# Regroupement des scripts:
 
#### 01_Restore/00_apt_source
[00_Restore_Apt_No_Recommends.sh](01_Restore/00_apt_source/00_Restore_Apt_No_Recommends.sh)
```bash
#!/bin/bash

FILE="/etc/apt/apt.conf.d/00pas-de-recommends"
LINE1='APT::Install-Recommends "false";'
LINE2='APT::Install-Suggests "false";'

if [[ ! -f "$FILE" ]]; then
    sudo touch "$FILE"
fi

if ! grep -qF "$LINE" "$FILE"; then
    echo "$LINE1"  | sudo tee -a $FILE >/dev/null
fi

if ! grep -qF "$LINE" "$FILE"; then
    echo "$LINE2"  | sudo tee -a $FILE >/dev/null
fi


```

[00_Restore_Apt_Sources_Bookworm.sh](01_Restore/00_apt_source/00_Restore_Apt_Sources_Bookworm.sh)
```bash
#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
export DEBIAN_PRIORITY=critical
echo '* libraries/restart-without-asking boolean true' | sudo debconf-set-selections

sudo apt install "ca-certificates" -y --no-install-recommends
sudo apt install "systemd-timesyncd" -y --no-install-recommends

fichierSourcesApt="/etc/apt/sources.list"

sudo rm "$fichierSourcesApt"

content='# Fichier sources apt par Nono
#deb http://deb.debian.org/debian/ bookworm main contrib non-free non-free-firmware
deb http://ftp-stud.hs-esslingen.de/debian/ bookworm main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian bookworm main contrib non-free non-free-firmware

# deb http://deb.debian.org/debian-security/ bookworm-security main contrib non-free non-free-firmware
deb http://security.debian.org/debian-security bookworm-security main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian-security/ bookworm-security main contrib non-free non-free-firmware



#deb http://deb.debian.org/debian/ bookworm-updates main contrib non-free non-free-firmware
deb http://ftp-stud.hs-esslingen.de/debian/ bookworm-updates main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian bookworm-updates main contrib non-free non-free-firmware

#deb http://deb.debian.org/debian/ bookworm-backports main contrib non-free non-free-firmware
deb http://ftp-stud.hs-esslingen.de/debian/ bookworm-backports main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian bookworm-backports main contrib non-free non-free-firmware'

echo "$content" | sudo tee $fichierSourcesApt > /dev/null

sudo apt update
sudo apt-get dist-upgrade --yes
echo '* libraries/restart-without-asking boolean false' | sudo debconf-set-selections

sudo apt autoremove -y


```

 
#### 01_Restore/01_ssh
[01_ssh.sh](01_Restore/01_ssh/01_ssh.sh)
```bash
#!/bin/bash

# Générer clé publique : 'ssh-keygen -t ed25519'

clear

# Installation de ssh, si nécessaire
if ! which ssh > /dev/null 2>&1; then
    echo "ssh n'est pas installé"
    echo "Installation de ssh, un peu de patience..."
    sudo apt-get update > /dev/null 2>&1
    sudo apt-get install ssh -y --no-install-recommends > /dev/null 2>&1
    sudo apt-get install sshfs -y --no-install-recommends > /dev/null 2>&1
    echo "ssh est installé!"
    echo "---"
fi

# Creation du dossier '.ssh', si nécessaire
ssh_dir="$HOME/.ssh"
if [ ! -d "$ssh_dir" ]; then
    echo "Création du dossier $ssh_dir"
    mkdir -p "$ssh_dir"
    echo "dossier '.ssh' créé!"
    echo "---"
fi

# Création di fichier 'authorized_keys' si nécessaire
authorized_keys="$ssh_dir/authorized_keys"
if [ ! -f "$authorized_keys" ]; then
    echo "Création du fichier $authorized_keys"
    touch "$authorized_keys"
    echo "fichier $authorized_keys créé!"
    echo "---"

fi

# Clés 'PC-Fixe'
if ! grep -q "nono@PC-Fixe" "$authorized_keys"; then 
    echo "Installation de la clé d'authentification pour 'nono@PC-Fixe'"
    echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCpTJX1DAgnUjC22cMKJTP5ePguOKyDjAs7wV3EeEsFHA51Sz/2/FMSjdyS6OOhLv63QsdGpi/kS8KK4nF7k6ODDzov5ki2UB45VJ7lFXqWbSIywUrEDr2VVrckStuFwUz71kD9OvK4qMV6eMHin9ynD2YhQUmC5ss6AJF98ldVk4nU+xsMy5ILXewJe+XmUeuyx6v8jPaJnaIkcKbsmIqfD/yokxNCYtEWZZSMWTsM2fTVgiMCgqJrcXfV40Y/Gpp35cKRLp4E2HuV4jthKi7p+Tcwjd+SfeWiAFecdW245/tobazf8PmPwwrDUAyUj+TOpJ1gx6zso2gE8kxJuLT1rTUSDrwuTNyNCFTT1g8olHDZgfEz8qRN5+ilpZTBZA7uyjkLWAj5P/3zZFT75T2zvXqKSI+/xrQeFvQOE7LlylU/tTqRa+TZZkGczrJ3zFshv79jtRaBsBhyyucwdfQR5V/l7lSbmyRbNmGmYtasR/QsQ/oSZqJLh22gzLpozPU= nono@PC-Fixe
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDxtl6lK9nKpK93otiNmCrLgpXqKNW5uhlRjfJ/Zlqojadm55SZ8BnbckqtZo7rdscBA+8+bQAtmltVI4enIOkDs4CHO5UMewnlyNgmVoEzmG3mstlXgvc+UB8OubCUw3bp4+9GJcxx7fOzQpwUz3KGoJU4QxssA7U2AXNt0xT6kK2/S3hFRQb334x8plXD2sLfr0Th6GtLiyDhjIrVdGmXVywQJWmiR86vXUWDcWmU9OAK+5fn2+d+1a8pFykaZACIZBx6unxik+7/9Wq5JEHJqfJ1sLkUFBc9aEZWf3gNIZbSFzNIa2GQegqH9kCGQHFwgwfeFevYUcDkosZzjcOi2+d9aK9B7493zLX87UxMfmTKX6tma6rmiixBnsj+Ch9ZZi5ElhzxXg3QDsdXPbr124se125bU8MS39cx21dFEVR6IWCII7Z3PX5tGZwYxyh0NTJPyKV1lcvLp6vcFm4y0GtcFX77cu0CQOMf+l1jKtq8tSxXBElKpUuxhc0SmJM= nono@PC-Fixe' >> "$authorized_keys"
    echo "clés pc-fixe autorisées"
    echo "---"
fi

# Clés pc-portable
if ! grep -q "nono@debian-portable" "$authorized_keys"; then 
    echo "Installation de la clé d'authentification pour 'nono@debian-portable'"
    echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCYkzOBG0w9Rsaptrx0fbDY9RizZtr4U+XuplGy90mpLvnVTtSGy6ChPsu0hbq19H407kUbkiTN5YXN51AtUKGro8aCHWuDrmECKc8bxq+CoeeD1JnaeYw55rbXIsGAMtTpFHnSrHKNtGn3xlWp2U6SBYczP6XjX90LRENKCCEZZMMfo201g670JDK10OnK8Pu3IUO1VlQilwUT4yYf3aV+J1lJ7KyabUhlESNgtIiKy2lWwW/DLVZ39mZ7H1XQN1lajO/XkHKcFuylTe10PNNZRLP4CO48HpSz4xf7d0DEJ2kjzWLxFYbVGo3ZIoBpgAXhR65k1Vi0nr9pf29xvrspcMolzNyFIBuYlUFIrRaCr8mU4+HY5dKMuS4sKdwO6OQ+yPEnZvBTasSpjl/0oGXnysU4PL1ud+HqY5O5GJDxRrs1BM6eIjtv4130ZdsnWQGpXeKsafHoOG568aFN4n/xi3b3pbJ1as97NwHdlDD3bUqssFe9i6MwIxxYv7hR0rbFvG8ZFWWY3FCorf1l4CdpdNKmD+iW27kkKJFHExXL79vPO7+CmXtp6OHWT8QVgWkeYxxOGa5LZm6w+hQwTBFORE21Q6t0gX9+92Igm6OWtOIc+jeSuxYM4nTEryNnGvJYka4Ah82ConeZH7/pSnNHKBkWJA4NedXPnQ1IeRjQfw== nono@debian-portable
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGXxmWsb7byzBVhhrAXvTpbT/9mFw7oHFuaSn6xdAc7o nono@debian-portable
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDkF/chjInKBzTl3GFsZ2EbKrXQjq1/LUhpxhhmPE2FFh/Smj0sSC+TFh36HzeKgTgyhdCJ3wpXAMBvie9Q9zHNrI86iCs6DkETK/hXQ8QRI0YP6C71fQO+KSO4VUZp+zEbnFIHxTA0/gdR2Fg9hG+779e3kaCbA4MWnCG/mXpemdsT3440RHbyxWs+TOzOLWuD5KNQ3x4Ziloaod5Yl8k8j6dMRtCidaVr4XbRQQUqwFk2svkDUCvCYqUTCNIaZ+YA7mYh0s6g/SzC1mz9aYDhPVVGf23zojos2Qnad0/qtgfCPXIHAQN+ERtkPE2wopLoSvFJjzVa8uvpXLiKwqdBYHkP7uMo/6Dgdc5+9LVEzTjuS4+jZIbG5P2rVuWmNYy+lmhuYC3eyMtfum0cZRcYGzJs7H8f6+hN1C8AAGe/2BaFqtn+cKi0jxbcDm2WheqYck0TjvKrGNYuJVYUMUxY4fwU4SdQ+BzpWLSaFYi4lLsoz0w7+RLSx9GxjxoDHa1vc1oGTBFf92e3Jl5xe5tGh6vATPd2pODLbbLCbcUSb7hiteKfyQBT2NJewm1NW/wLl/sOkyvu+CDAqursDX7AJ+djbGl09aM8yThqq5SxoVxBwK37JhLPBkwOu9Ba0RR55FJRODRs9ASQC0Xo+E+42ZEX73e9CMr9tMfOsnDDrQ== ar.delsaut@outlook.com' >> "$authorized_keys"
    echo "clés pc-portable autorisées"
    echo "---"
fi


# Édition fichier sshd_config
file="/etc/ssh/sshd_config"
if [ -f "$file" ]; then
    grep "#AllowTcpForwarding yes" "$file" >/dev/null
    if [ $? -eq 0 ]; then
        echo "Modification du fichier '$file'"
        sudo mv "$file" "$file.old"
        echo 'Include /etc/ssh/sshd_config.d/*.conf
KbdInteractiveAuthentication no
UsePAM yes
AllowAgentForwarding yes
AllowTcpForwarding yes
X11Forwarding yes
X11DisplayOffset 10
X11UseLocalhost no
PrintMotd no
AcceptEnv LANG LC_*
Subsystem	sftp	/usr/lib/openssh/sftp-server' | sudo tee "$file" >/dev/null
    echo "Fichier '$file' modifié!"
    echo "---"
    fi
fi

# Édition fichier .ssh/config
file="$HOME/.ssh/config"
if [ ! -f "$file" ]; then
    echo "modification du fichier '.ssh/config'"
    echo 'Host *
    ForwardAgent yes
    ForwardX11 yes
    ' | tee "$file"  >/dev/null
    echo "Fichier .shh/config modifié!"
    echo "---"
fi


```

 
#### 01_Restore/02_Nas
[01_Nas.sh](01_Restore/02_Nas/01_Nas.sh)
```bash
#!/bin/bash

clear

    # Titre
echo "Connexion au Nas:"
echo "---"

    # Vérification des packages .deb
dpkg -l | grep "cifs-utils" >/dev/null
if [ $? -eq 1 ]; then
    echo "Paquet 'cifs-utils' non installé"
    echo "Installation du paquet 'cifs-utils'"
    echo "Mise à jour des sources apt en cours ..."
    sudo apt-get update > /dev/null 2>&1
    echo "Installation du paquet 'cifs-utils' en cours..."
    sudo apt-get install cifs-utils -y --no-install-recommends > /dev/null 2>&1
    echo "Paquet 'cifs-utils' installé!"
    echo "---"
fi

    # Création du dossier credential
dir_NonoOS_Nas="$HOME/.config/NonoOS/Nas"
if [ ! -d "$dir_NonoOS_Nas" ]; then
    echo "Création du dossier '$dir_NonoOS_Nas'."
    sudo mkdir -p "$dir_NonoOS_Nas"  
    echo "Dossier credential créé!"
    echo "---"
fi

    # Création du fichier credential
fichierCredential="$dir_NonoOS_Nas/nas_credentials"
if [ ! -f "$fichierCredential" ]; then
    echo "Création du fichier credentials $fichierCredential..."
    echo "username=Nono" | sudo tee "$fichierCredential"
    echo "password=Poul€44211030" | sudo tee -a "$fichierCredential"
    echo "Fichier credential créé!"
    echo "---"
fi


paths="/mnt/nas/archives /mnt/nas/downloads /mnt/nas/drive /mnt/nas/video"
for path in $paths
    do
    file_name=$(basename "$path")
    file_name_no_ext="${file_name%.*}"
    file_path="/etc/systemd/system/mount-nas-$file_name_no_ext.service"
    if [ ! -d "$path" ]; then
        echo "Création du dossier de montage pour: $path"
        echo "---"
        sudo mkdir -p "$path"
        echo "Dossier $path: OK"
        echo "---"
    fi

    if [ ! -f "$file_path" ]; then
        echo "Création du fichier service: $file_path"
        echo "---"
        sudo tee "$file_path" > /dev/null <<EOF
[Unit]
Description=Mount Nas-$file_name_no_ext at startup
After=network-online.target
Wants=network-online.target

[Service]
Type=oneshot
ExecStartPre=/bin/sleep 5
ExecStart=/bin/mount -t cifs //nonobouli.myds.me/$file_name_no_ext $path -o credentials=$fichierCredential,uid=$(id -u),gid=$(id -g),file_mode=0644,dir_mode=0755,vers=3.0

[Install]
WantedBy=multi-user.target
EOF
        echo "Fichier $file_path: OK"
        echo "---"
    fi

    if ! systemctl is-enabled "mount-nas-$file_name_no_ext.service" &> /dev/null; then
        echo "activation du service mount-nas-$file_name_no_ext.service"
        echo "---"
        sudo systemctl enable "mount-nas-$file_name_no_ext.service"
        echo "---"
    fi
done

sudo systemctl daemon-reload
if [ -z "$(ls -A /mnt/nas/archives/)" ] || systemctl is-active mount-nas-archives.service | grep -q "failed"; then
    echo "Directory is empty"
    sudo umount /mnt/nas/archives/ &> /dev/null
    sudo systemctl restart --now "mount-nas-archives.service" &
fi

if [ -z "$(ls -A /mnt/nas/drive/)" ] || systemctl is-active mount-nas-drive.service | grep -q "failed"; then
    echo "Directory is empty"
    sudo umount /mnt/nas/drive/ &> /dev/null
    sudo systemctl restart --now "mount-nas-drive.service" &
fi

if [ -z "$(ls -A /mnt/nas/video/)" ] || systemctl is-active mount-nas-video.service | grep -q "failed"; then
    echo "Directory is empty"
    sudo umount /mnt/nas/video/ &> /dev/null
    sudo systemctl restart --now "mount-nas-video.service" &
fi

if [ -z "$(ls -A /mnt/nas/downloads/)" ] || systemctl is-active mount-nas-downloads.service | grep -q "failed"; then
    echo "Directory is empty"
    sudo umount /mnt/nas/downloads/ &> /dev/null
    sudo systemctl restart --now "mount-nas-downloads.service" &
fi

echo "Dossiers Nas montés"
echo "---"


```

 
#### 01_Restore/03_dossier_config
[01_Dossier_Config.sh](01_Restore/03_dossier_config/01_Dossier_Config.sh)
```bash
#!/bin/bash

clear

dossierBackup="/mnt/nas/archives/PC/Linux/Backup_Home"



while true; do
    clear
    echo "en attente que le nas soit disponnible et bien monté"
    clear
    if [ "$(find "$dossierBackup" -mindepth 1 -print -quit)" ]; then
        echo "Dossier nas accessible!"
        break
    fi
    sleep 0.1
done

sudo rm -rf $HOME

latest_tar=$(ls -lt /mnt/nas/archives/PC/Linux/Backup_Home/*.tar | awk '{print $NF}' | head -n 1)

sudo tar -xvf "$latest_tar" -C /

if [ -e "$HOME/.local/state/wireplumber" ]; then
    rm -rf "$HOME/.local/state/wireplumber"
fi

sudo rm -rf $HOME/.cache

sudo rm -rf "/home/nono/.var/app/com.ktechpit.whatsie"

cd /
cd $HOME

# Création des dossiers utilisateur de base
# xdg-user-dirs-update


```

 
#### 01_Restore/04_autres
[00_Restore_Wifi.sh](01_Restore/04_autres/00_Restore_Wifi.sh)
```bash
#!/bin/bash

clear

PACKAGE="network-manager"
if ! apt -qq list $PACKAGE --installed 2>/dev/null | grep -q "$PACKAGE"; then
    sudo apt install $PACKAGE  -y --no-install-recommends 
fi


sudo sed -i 's/^managed=false$/managed=true/' /etc/NetworkManager/NetworkManager.conf

# Pour préparer le wifi
# sudo sed -i '/# The primary network interface/,$d' /etc/network/interfaces
# Désactivation de default internet au profit de NetworkManager
sudo mv /etc/network/interfaces /etc/network/interfaces.old
content="# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
#allow-hotplug wlp0s20f3
#iface wlp0s20f3 inet dhcp
#       wpa-ssid Nono_5GHz
#       wpa-psk  bace090890
"
echo "$content" | sudo tee /etc/network/interfaces >/dev/null

# fichiers wifi
path_Gsm="/etc/NetworkManager/system-connections/Galaxy_S10e66de.nmconnection"
if [ ! -e "$path_Gsm" ]; then
    ssid_Gsm="Galaxy_S10e66de"
    password_Gsm="abcd1234"

    basename=$(basename "$path_Gsm")
    basename_no_ext="${basename%.*}"

    echo "[connection]
id=$basename_no_ext
uuid=d65f4629-64a4-4ef7-b5bd-4b05fe6823a0
type=wifi

[wifi]
mode=infrastructure
ssid=$ssid_Gsm

[wifi-security]
key-mgmt=wpa-psk
psk=$password_Gsm

[ipv4]
method=auto

[ipv6]
addr-gen-mode=default
method=auto

[proxy]
" |sudo tee -a "$path_Gsm"

    sudo chown root:root "$path_Gsm"
    sudo chmod 600 "$path_Gsm"
fi


path_Nono_5GHz="/etc/NetworkManager/system-connections/Nono_5GHz.nmconnection"
if [ ! -e "$path_Nono_5GHz" ]; then
ssid_Nono_5GHz="Nono_5GHz"
password_Nono_5GHz="bace090890"

basename=$(basename "$path_Nono_5GHz")
basename_no_ext="${basename%.*}"

echo "[connection]
id=$basename_no_ext
uuid=d65f4629-64a4-4ef7-b5bd-4b05fe6823a0
type=wifi

[wifi]
mode=infrastructure
ssid=$ssid_Nono_5GHz

[wifi-security]
key-mgmt=wpa-psk
psk=$password_Nono_5GHz

[ipv4]
method=auto

[ipv6]
addr-gen-mode=default
method=auto

[proxy]
" |sudo tee -a "$path_Nono_5GHz"

sudo chown root:root "$path_Nono_5GHz"
sudo chmod 600 "$path_Nono_5GHz"
fi


```

[01_Restore_Audio.sh](01_Restore/04_autres/01_Restore_Audio.sh)
```bash
#!/bin/bash

clear

# Audio
echo '
pipewire
wireplumber
pipewire-alsa
pipewire-jack
pipewire-pulse
pulseaudio-utils
#pulseaudio-module-raop
#pulseaudio-module-zeroconf
alsa-firmware-loaders
pavucontrol
libspa-0.2-modules
' >> "/tmp/packagelist_NonoOS.txt"

sed -i 's/#.*//' /tmp/packagelist_NonoOS.txt
sed -i '/^[[:space:]]*$/d' /tmp/packagelist_NonoOS.txt
chmod +x /tmp/packagelist_NonoOS.txt
sudo xargs apt install -y --no-install-recommends < /tmp/packagelist_NonoOS.txt
rm -rf /tmp/packagelist_NonoOS.txt

# LANG=C pactl info | grep '^Server Name'

if [ -e "$HOME/.local/state/wireplumber" ]; then
    rm -rf "$HOME/.local/state/wireplumber"
fi

# Pulseaudio
file="/etc/pulse/default.pa"
line="load-module module-raop-discover"
if [ -f "$file" ]; then
    if ! grep -Fxq "$line" "$file"; then
        echo "$line" | sudo tee -a "$file" >/dev/null
    fi
fi

pactl load-module module-raop-discover

# sudo apt install paprefs -y --no-install-recommends
# sudo ln -s /usr/lib/pulse-16.1+dfsg1 /usr/lib/pulse-16.1


```

[03_Restore_polkit-a_No_Password.sh](01_Restore/04_autres/03_Restore_polkit-a_No_Password.sh)
```bash
#!/bin/bash

clear

sudo apt install policykit-1 policykit-1-gnome polkitd -y --no-install-recommends 

file_path="/etc/polkit-1/rules.d/49-nopasswd_limited.rules"

content='/* Allow members of the wheel group to execute any actions
 * without password authentication, similar to "sudo NOPASSWD:"
 */
polkit.addRule(function(action, subject) {
    if (subject.isInGroup("sudo")) {
        return polkit.Result.YES;
    }
});'


if [ ! -f "$file_path" ]; then
    echo "$content" | sudo tee "$file_path" >/dev/null
else
    mv "$file_path" "$file_path.old"
    echo "$content" | sudo tee "$file_path" >/dev/null
fi 


```

[04_Restore_Themes.sh](01_Restore/04_autres/04_Restore_Themes.sh)
```bash
#!/bin/bash

clear

# Installation du curseur de theme
sudo apt-get install -y "papirus-icon-theme" --no-install-recommends 
sudo apt-get install -y "bibata-cursor-theme" --no-install-recommends 
sudo update-alternatives --install /usr/bin/x-cursor-theme x-cursor-theme /usr/share/icons/Bibata-Modern-Classic/cursor.theme 95
sudo update-alternatives --auto x-cursor-theme
lines="[Icon Theme]\nInherits=Bibata-Modern-Classic"
file="/usr/share/icons/default/index.theme"
if [ ! -f "$file" ]; then
    sudo touch "$file"
    sudo bash -c "echo -e '$lines' >> '$file'"
fi


```

[06_Restore_Grub_config.sh](01_Restore/04_autres/06_Restore_Grub_config.sh)
```bash
#!/bin/bash

clear

if [ ! -e "/usr/share/pixmaps/Debian_ascii_1920.png" ]; then
    sudo cp -rf "$HOME/Images/Wallpapers/Debian_ascii_1920.png" "/usr/share/pixmaps"
fi

config_file="/etc/default/grub"
sudo sed -i 's/^GRUB_TIMEOUT=.*/GRUB_TIMEOUT=3/' "$config_file"
sudo sed -i 's/GRUB_DEFAULT=.*/GRUB_DEFAULT=saved/' "$config_file"
echo 'GRUB_SAVEDEFAULT=true' | sudo tee -a "$config_file"
echo "GRUB_BACKGROUND=/usr/share/pixmaps/Debian_ascii_1920.png" | sudo tee -a "$config_file"
sudo update-grub


```

[07_Restore_Dismiss_DPMS.sh](01_Restore/04_autres/07_Restore_Dismiss_DPMS.sh)
```bash
#!/bin/bash

clear

file_path="/etc/X11/xorg.conf.d/serverflags.conf"
if [ ! -f "$file_path" ]; then
    sudo mkdir -p "/etc/X11/xorg.conf.d"
    content='Section "ServerFlags"
#...
Option "BlankTime" "0"
Option "StandbyTime" "0"
Option "SuspendTime" "0"
Option "OffTime" "0"
EndSection'
    echo "$content" | sudo tee "$file_path" >/dev/null
fi


```

[08_Restore_Ram_On_File.sh](01_Restore/04_autres/08_Restore_Ram_On_File.sh)
```bash
#!/bin/bash

clear

# Creation de swap (8gb), un fichier a la racine du systeme
sudo fallocate -l 8G /swapfile >/dev/null
sudo chmod 600 /swapfile >/dev/null
sudo mkswap /swapfile >/dev/null
sudo swapon /swapfile >/dev/null
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab >/dev/null
sudo sysctl vm.swappiness=10 >/dev/null
echo 'vm.swappiness=10' | sudo tee -a /etc/sysctl.conf >/dev/null
sudo sysctl vm.vfs_cache_pressure=50 >/dev/null
echo 'vm.vfs_cache_pressure=50' | sudo tee -a /etc/sysctl.conf >/dev/null


```

[09_Restore_Brightness_udev_rules.sh](01_Restore/04_autres/09_Restore_Brightness_udev_rules.sh)
```bash
#!/bin/bash

clear

file="/usr/lib/udev/rules.d/90-brightnessctl.rules"


lines='ACTION=="add", SUBSYSTEM=="backlight", RUN+="bright-helper video g+w /sys/class/backlight/%k/brightness"
ACTION=="add", SUBSYSTEM=="leds",      RUN+="bright-helper input g+w /sys/class/leds/%k/brightness"'


if [ ! -f "$file" ]; then
    sudo touch "$file"
    sudo bash -c "echo -e '$lines' >> '$file'"
fi


```

[09_Restore_Sudo_No_Password.sh](01_Restore/04_autres/09_Restore_Sudo_No_Password.sh)
```bash
#!/bin/bash

clear

PACKAGE="sudo"
if ! apt -qq list $PACKAGE --installed 2>/dev/null | grep -q "$PACKAGE"; then
    sudo apt install $PACKAGE  -y --no-install-recommends 
fi



old_string="ALL=(ALL:ALL) ALL"
new_string="ALL=(ALL) NOPASSWD: ALL"

sudo sed -i "/^%sudo/ s/$old_string/$new_string/" "/etc/sudoers"


```

[10_Restore_Wallpaper.sh](01_Restore/04_autres/10_Restore_Wallpaper.sh)
```bash
#!/bin/bash

# Xfce4
if [[ "$XDG_CURRENT_DESKTOP" == *"XFCE"* ]]; then
    WALLPAPER_PATH="/usr/share/pixmaps/Debian_ascii_1920.png"
    if [ ! -e "$WALLPAPER_PATH" ]; then
        sudo cp -rf "$HOME/Images/Wallpapers/Debian_ascii_1920.png" "/usr/share/pixmaps"
    fi
    xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitoreDP-1/workspace0/last-image" -s "$WALLPAPER_PATH"
fi

# Cinnamon
if [[ "$XDG_CURRENT_DESKTOP" == *"cinnamon"* ]]; then
    if [ ! -e "$WALLPAPER_PATH" ]; then
        sudo cp -rf "$HOME/Images/Wallpapers/Debian_ascii_1920.png" "/usr/share/pixmaps"
    fi
    gsettings set org.cinnamon.desktop.background picture-uri "file://$WALLPAPER_PATH"
fi


```

[11_Restore_gmd3.sh](01_Restore/04_autres/11_Restore_gmd3.sh)
```bash
#!/bin/bash

clear

PACKAGE="gdm3"
if ! apt -qq list $PACKAGE --installed 2>/dev/null | grep -q "$PACKAGE"; then
    sudo apt install $PACKAGE  -y --no-install-recommends 
fi

conf_file="/etc/gdm3/daemon.conf"
if [ -f "$conf_file" ]; then
    if grep -q "\[daemon\]" "$conf_file"; then
        sudo sed -i '/\[daemon\]/a AutomaticLoginEnable = true' "$conf_file"
        sudo sed -i "/AutomaticLoginEnable = true/a AutomaticLogin = $USER" "$conf_file"
    fi
fi

search_line="AutomaticLogin = $USER"
count=$(grep -c "$search_line" "$conf_file")
if [ $count -gt 1 ]; then
    sudo sed -i '0,/'"$search_line"'/!{/^'"$search_line"'/d;}' "$conf_file"
fi

search_line="AutomaticLoginEnable = true"
count=$(grep -c "$search_line" "$conf_file")
if [ $count -gt 1 ]; then
    sudo sed -i '0,/'"$search_line"'/!{/^'"$search_line"'/d;}' "$conf_file"
fi


# Restart GDM3 for changes to take effect
# systemctl restart gdm3


```

[12_Restore_VirtualBox_guest.sh](01_Restore/04_autres/12_Restore_VirtualBox_guest.sh)
```bash
#!/bin/bash

product_name=$(sudo dmidecode -s system-product-name)

if [ "$product_name" = "VMware Virtual Platform" ]; then
    sudo apt-get install -y open-vm-tools
fi

if [ "$product_name" = "VirtualBox" ]; then
    ancien_path="$PWD"
    sudo apt install "fasttrack-archive-keyring" -y
    line="# Dépot 1 FastTrack pour Virtualbox
    deb https://fasttrack.debian.net/debian-fasttrack/ bookworm-fasttrack main contrib"
    if ! grep -qxF "$line" /etc/apt/sources.list; then
        echo "$line" | sudo tee -a /etc/apt/sources.list >/dev/null
    fi
    line="# Dépot 1 FastTrack pour Virtualbox
    deb https://fasttrack.debian.net/debian-fasttrack/ bookworm-backports-staging main contrib"
    if ! grep -qxF "$line" /etc/apt/sources.list; then
        echo "$line" | sudo tee -a /etc/apt/sources.list >/dev/null
    fi
    sudo apt update
    sudo apt install virtualbox-guest-additions-iso --no-install-recommends -y
    # sudo apt install virtualbox-guest-x11 --no-install-recommends -y
    sudo apt install virtualbox-guest-utils --no-install-recommends -y
    sudo apt install build-essential dkms linux-headers-$(uname -r)  --no-install-recommends -y
    sudo mkdir -p /mnt/iso
    sudo mount -o loop "/usr/share/virtualbox/VBoxGuestAdditions.iso" /mnt/iso 2>/dev/null
    cd /mnt/iso
    sudo ./VBoxLinuxAdditions.run
    cd $ancien_path
    sudo umount /mnt/iso 
    sudo apt remove virtualbox-guest-additions-iso -y
    # sudo reboot
fi


```

 
#### 01_Restore/05_Packages
[00_Packages_Desktop_Gnome.sh](01_Restore/05_Packages/00_Packages_Desktop_Gnome.sh)
```bash
#!/bin/bash


# Fixe erreur lors de 'apt install' après avoir delet $HOME
cd /
cd "$HOME"

echo '
gnome-core
gnome-tweaks
gnome-software-plugin-flatpak
fonts-dejavu
network-manager-gnome
gnome-shell-extensions
gnome-shell-extension-appindicator
gnome-shell-extension-hide-activities
#gnome-shell-extension-arc-menu
gnome-shell-extension-autohidetopbar
#gnome-shell-extension-bluetooth-quick-connect
gnome-shell-extension-caffeine
gnome-shell-extension-manager
gnome-shell-extension-no-annoyance
#gnome-shell-extension-dashtodock
#gnome-shell-extension-dash-to-panel
gnome-shell-extension-gpaste                   
gnome-shell-extension-tiling-assistant
gnome-shell-extension-gsconnect
gnome-shell-extension-gsconnect-browsers
#gnome-shell-extension-weather
nautilus-admin
' >> /tmp/packagelist_NonoOS.txt

sed -i 's/#.*//' /tmp/packagelist_NonoOS.txt
sed -i '/^[[:space:]]*$/d' /tmp/packagelist_NonoOS.txt
chmod +x /tmp/packagelist_NonoOS.txt
sudo xargs apt install -y --no-install-recommends < /tmp/packagelist_NonoOS.txt
rm -rf /tmp/packagelist_NonoOS.txt


```

[01_Packages_Desktop_All.sh](01_Restore/05_Packages/01_Packages_Desktop_All.sh)
```bash
#!/bin/bash

# set -e

# Fixe erreur lors de 'apt install' après avoir delet $HOME
cd /
cd "$HOME"



# # Pulseaudio
# pulseaudio
# pulseaudio-module-raop
# pulseaudio-module-zeroconf
# pcmanfm


# Video
echo '
xserver-xorg
xserver-xorg-core
xserver-xorg-video-intel
xserver-xorg-input-libinput
x11-apps
x11-utils
xarchiver
xdg-desktop-portal-gtk
xdo
xdotool
xinput
' >> "/tmp/packagelist_NonoOS.txt"


# Applications
echo '
fzf
htop
inkscape
git
guake
micro
xclip
mpv
thunderbird
# thunderbird-gtk3
thunderbird-l10n-fr
' >> "/tmp/packagelist_NonoOS.txt"


# Theme
echo '
adwaita-qt
adwaita-qt6
papirus-icon-theme
bibata-cursor-theme
gnome-themes-extra-data
qt5ct
' >> "/tmp/packagelist_NonoOS.txt"


# Fonts
echo '
fonts-cantarell
fonts-dejavu-core
fonts-font-awesome
ttf-mscorefonts-installer
ttf-xfree86-nonfree
' >> "/tmp/packagelist_NonoOS.txt"


# Langues
echo '
hunspell-fr
hunspell-fr-classical
task-french
task-french-desktop
' >> "/tmp/packagelist_NonoOS.txt"


# Firmware
echo '
firmware-b43-installer
firmware-b43legacy-installer
firmware-linux
firmware-linux-nonfree
' >> "/tmp/packagelist_NonoOS.txt"


# Systèmes
echo '
accountsservice
acpi
bash
bash-completion
bat
brightnessctl
cifs-utils
curl
dbus
dbus-bin
dbus-daemon
dbus-session-bus-common
dbus-system-bus-common
dbus-user-session
debconf-utils
fd-find
ffmpeg
gnome-keyring
gstreamer1.0-plugins-bad
gstreamer1.0-plugins-ugly
gtk2-engines-murrine
gvfs
gvfs-backends
gvfs-fuse
haveged
lsb-release
network-manager
nmap
ntfs-3g
pigz
pkexec
rofi
tumbler
user-setup
wget
whois
wpasupplicant
' >> "/tmp/packagelist_NonoOS.txt"


# Bluetooth
echo '
blueman
bluetooth
bluez
bluez-cups
bluez-firmware
bluez-obexd
libspa-0.2-bluetooth
' >> "/tmp/packagelist_NonoOS.txt"


sed -i 's/#.*//' /tmp/packagelist_NonoOS.txt
sed -i '/^[[:space:]]*$/d' /tmp/packagelist_NonoOS.txt
chmod +x /tmp/packagelist_NonoOS.txt
sudo xargs apt install -y --no-install-recommends < /tmp/packagelist_NonoOS.txt
rm -rf /tmp/packagelist_NonoOS.txt


# Pour lire dvd, mais demande interaction utilisateur, a fixer
#libdvd-pkg


# fonts-powerline
# powerline-daemon -q
# POWERLINE_BASH_CONTINUATION=1
# POWERLINE_BASH_SELECT=1
# source /usr/share/powerline/bindings/bash/powerline.sh


```

[02_Packages_deb_manual.sh](01_Restore/05_Packages/02_Packages_deb_manual.sh)
```bash
#!/bin/bash

#### VSCodium #### -----------------------------------------------------------------------------
sudo wget https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg -O /usr/share/keyrings/vscodium-archive-keyring.asc
echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.asc ] https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs vscodium main'     | sudo tee /etc/apt/sources.list.d/vscodium.list
sudo apt-get update
sudo apt-get install codium -y

#### Firefox #### -----------------------------------------------------------------------------
sudo install -d -m 0755 /etc/apt/keyrings
wget -q https://packages.mozilla.org/apt/repo-signing-key.gpg -O- | sudo tee /etc/apt/keyrings/packages.mozilla.org.asc > /dev/null
echo "deb [signed-by=/etc/apt/keyrings/packages.mozilla.org.asc] https://packages.mozilla.org/apt mozilla main" | sudo tee -a /etc/apt/sources.list.d/mozilla.list > /dev/null
echo '
Package: *
Pin: origin packages.mozilla.org
Pin-Priority: 1000
' | sudo tee /etc/apt/preferences.d/mozilla
sudo apt-get update && sudo apt-get install firefox -y

#### Parsec #### -----------------------------------------------------------------------------
LIBJPEG8_PACKAGE_URL="https://archive.debian.org/debian/pool/main/libj/libjpeg8/libjpeg8_8b-1_amd64.deb"
LIBJPEG8_PACKAGE_FILENAME="/tmp/libjpeg8_8b-1_amd64.deb"
wget "$LIBJPEG8_PACKAGE_URL" -O "$LIBJPEG8_PACKAGE_FILENAME"
sudo apt-get install "$LIBJPEG8_PACKAGE_FILENAME" -y
rm "$LIBJPEG8_PACKAGE_FILENAME"
PARSEC_PACKAGE_URL="https://builds.parsec.app/package/parsec-linux.deb"
PARSEC_PACKAGE_FILENAME="/tmp/parsec-linux.deb"
wget "$PARSEC_PACKAGE_URL" -O "$PARSEC_PACKAGE_FILENAME"
sudo apt-get install "$PARSEC_PACKAGE_FILENAME" -y
rm "$PARSEC_PACKAGE_FILENAME"

#### Touchegg #### -----------------------------------------------------------------------------
owner="JoseExposito"
repo="touchegg"
latest_release=$(wget -qO- "https://api.github.com/repos/$owner/$repo/releases/latest")
url=$(echo "$latest_release" | grep -oP '"browser_download_url": "\K([^"]+\.deb)' | head -n 1)
if [ -n "$url" ]; then
    wget "$url" -P /tmp
    sudo apt-get install -y "/tmp/$(basename "$url")"
    rm -f "/tmp/$(basename "$url")"
fi

#### Synology Drive #### -----------------------------------------------------------------------------
URL="https://archive.synology.com/download/Utility/SynologyDriveClient/"
HTML_CONTENT=$(curl -s "$URL")
LATEST_VERSION=$(echo "$HTML_CONTENT" | grep -oE 'SynologyDriveClient/[0-9]+\.[0-9]+\.[0-9]+-[0-9]+' | head -n1)
VERSION_NUMBER=${LATEST_VERSION##*-}
wget "https://global.synologydownload.com/download/Utility/$LATEST_VERSION/Ubuntu/Installer/synology-drive-client-$VERSION_NUMBER.x86_64.deb" -P /tmp
sudo apt-get install -y "/tmp/synology-drive-client-$VERSION_NUMBER.x86_64.deb"
rm -f "/tmp/synology-drive-client-$VERSION_NUMBER.x86_64.deb"

#### Steam #### -----------------------------------------------------------------------------
sudo dpkg --add-architecture i386
sudo apt update
sudo apt install \
"steam-libs-i386" \
"steam-installer" \
"steam-libs" \
-y \
--no-install-recommends


```

[03_Packages_Flatpak.sh](01_Restore/05_Packages/03_Packages_Flatpak.sh)
```bash
#!/bin/bash
if ! command -v flatpak &> /dev/null; then
    sudo apt update
    sudo apt install flatpak -y --no-install-recommends
    sudo apt install flatpak-xdg-utils -y --no-install-recommends
fi

sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
sudo flatpak install \
"md.obsidian.Obsidian" \
"com.ktechpit.whatsie" \
"com.discordapp.Discord" \
"io.github.shiftey.Desktop" \
"org.nomacs.ImageLounge" \
-y 

# "com.synology.SynologyDrive"


```

[99_Nettoyage_apres_install.sh](01_Restore/05_Packages/99_Nettoyage_apres_install.sh)
```bash
#!/bin/bash

xdg-mime default pcmanfm.desktop inode/directory

#  Update Fonts
sudo fc-cache -f -v

# Nettoyage de apt
sudo apt autoclean -y
sudo apt autoremove -y

# Redémarrage
# sudo reboot


```

 
#### 01_Restore/not_install/apt
[00_Restore_Apt_Sources_sid.sh](01_Restore/not_install/apt/00_Restore_Apt_Sources_sid.sh)
```bash
#!/bin/bash

# export NEEDRESTART_MODE=a
export DEBIAN_FRONTEND=noninteractive
## Questions that you really, really need to see (or else). ##
export DEBIAN_PRIORITY=critical
echo '* libraries/restart-without-asking boolean true' | sudo debconf-set-selections

# Mise à jour de la liste de sources debian:
# sudo apt update && sudo apt upgrade -y

# sudo apt full-upgrade -y

sudo apt install "ca-certificates" -y --no-install-recommends
sudo apt install "systemd-timesyncd" -y --no-install-recommends

fichierSourcesApt="/etc/apt/sources.list"

sudo rm "$fichierSourcesApt"

content='# Fichier sources apt par Nono
deb https://ftp.debian.org/debian/ sid contrib main non-free non-free-firmware'

# content='# Fichier sources apt par Nono
# deb http://deb.debian.org/debian unstable main
# deb http://deb.debian.org/debian-debug unstable-debug main
# deb http://deb.debian.org/debian-ports unstable main'

echo "$content" | sudo tee $fichierSourcesApt > /dev/null

sudo apt-get update
sudo apt-get dist-upgrade --yes
echo '* libraries/restart-without-asking boolean false' | sudo debconf-set-selections

sudo apt autoremove
```

[00_Restore_Apt_Sources_Trixie.sh](01_Restore/not_install/apt/00_Restore_Apt_Sources_Trixie.sh)
```bash
#!/bin/bash

# export NEEDRESTART_MODE=a
export DEBIAN_FRONTEND=noninteractive
## Questions that you really, really need to see (or else). ##
export DEBIAN_PRIORITY=critical
echo '* libraries/restart-without-asking boolean true' | sudo debconf-set-selections

# Mise à jour de la liste de sources debian:
# sudo apt update && sudo apt upgrade -y

# sudo apt full-upgrade -y

sudo apt install "ca-certificates" -y --no-install-recommends
sudo apt install "systemd-timesyncd" -y --no-install-recommends

fichierSourcesApt="/etc/apt/sources.list"

sudo rm "$fichierSourcesApt"

content='# Fichier sources apt par Nono
#deb http://deb.debian.org/debian/ trixie main contrib non-free non-free-firmware
deb http://ftp-stud.hs-esslingen.de/debian/ trixie main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian trixie main contrib non-free non-free-firmware

# deb http://deb.debian.org/debian-security/ trixie-security main contrib non-free non-free-firmware
deb http://security.debian.org/debian-security trixie-security main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian-security/ trixie-security main contrib non-free non-free-firmware



#deb http://deb.debian.org/debian/ trixie-updates main contrib non-free non-free-firmware
deb http://ftp-stud.hs-esslingen.de/debian/ trixie-updates main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian trixie-updates main contrib non-free non-free-firmware

#deb http://deb.debian.org/debian/ trixie-backports main contrib non-free non-free-firmware
deb http://ftp-stud.hs-esslingen.de/debian/ trixie-backports main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian trixie-backports main contrib non-free non-free-firmware'

echo "$content" | sudo tee $fichierSourcesApt > /dev/null


sudo apt-get update
sudo apt-get dist-upgrade --yes
echo '* libraries/restart-without-asking boolean false' | sudo debconf-set-selections

sudo apt autoremove

# deb https://deb.debian.org/debian/ testing contrib main non-free non-free-firmware
# deb https://deb.debian.org/debian/ testing-updates contrib main non-free non-free-firmware
# deb https://deb.debian.org/debian/ testing-proposed-updates contrib main non-free non-free-firmware
# deb https://deb.debian.org/debian/ testing-backports contrib main non-free non-free-firmware
# deb https://security.debian.org/debian-security/ testing-security contrib main non-free non-free-firmware'


```

 
#### 01_Restore/not_install/desktop_environments
[00_Packages_Desktop_Bspwm.sh](01_Restore/not_install/desktop_environments/00_Packages_Desktop_Bspwm.sh)
```bash
#!/bin/bash


# Fixe erreur lors de 'apt install' après avoir delet $HOME
cd /
cd "$HOME"

echo '
bspwm
dunst
feh
i3lock-fancy
lightdm
numlockx
polybar
spacefm
sxhkd
xautolock
dkms
libinput-tools
picom
kitty
' >> /tmp/packagelist_NonoOS.txt

sed -i 's/#.*//' /tmp/packagelist_NonoOS.txt
sed -i '/^[[:space:]]*$/d' /tmp/packagelist_NonoOS.txt
chmod +x /tmp/packagelist_NonoOS.txt
sudo xargs apt install -y --no-install-recommends < /tmp/packagelist_NonoOS.txt
rm -rf /tmp/packagelist_NonoOS.txt


```

[00_Packages_Desktop_Xfce4.sh](01_Restore/not_install/desktop_environments/00_Packages_Desktop_Xfce4.sh)
```bash
#!/bin/bash


# Fixe erreur lors de 'apt install' après avoir delet $HOME
cd /
cd "$HOME"

echo '
lightdm
xfce4
xfce4-notifyd
xfce4-power-manager-plugins
xfce4-terminal
xfce4-whiskermenu-plugin
mugshot
librsvg2-common 
' >> /tmp/packagelist_NonoOS.txt

sed -i 's/#.*//' /tmp/packagelist_NonoOS.txt
sed -i '/^[[:space:]]*$/d' /tmp/packagelist_NonoOS.txt
chmod +x /tmp/packagelist_NonoOS.txt
sudo xargs apt install -y --no-install-recommends < /tmp/packagelist_NonoOS.txt
rm -rf /tmp/packagelist_NonoOS.txt


```

 
#### 01_Restore/not_install/login_manager
[11_Restore_Lightdm_config.sh](01_Restore/not_install/login_manager/11_Restore_Lightdm_config.sh)
```bash
#!/bin/bash

clear

sudo apt install  lightdm -y --no-install-recommends

if [ ! -e "/usr/share/pixmaps/Debian_ascii_1920.png" ]; then
    sudo cp -rf "$HOME/Images/Wallpapers/Debian_ascii_1920.png" "/usr/share/pixmaps"
fi

if [ ! -e "/usr/share/pixmaps/avatar_icone.ico" ]; then
    sudo cp -rf "$HOME/Images/icons/avatar_icone.ico" "/usr/share/pixmaps"
fi

# Edition de /etc/lightdm/lightdm.conf
    # AutoLogin
sudo sed -i "/^#*autologin-user=/c\autologin-user=$USER" /etc/lightdm/lightdm.conf
sudo sed -i "/^#*autologin-user-timeout=/c\autologin-user-timeout=0" /etc/lightdm/lightdm.conf
    # Voir noms utilisateurs
sudo sed -i "/^#*greeter-hide-users=false/c\greeter-hide-users=false" /etc/lightdm/lightdm.conf
    # Activer numlock
sudo sed -i "/^#*greeter-setup-script=/c\greeter-setup-script=/usr/bin/numlockx on" /etc/lightdm/lightdm.conf

#  Édition de /etc/lightdm/lightdm-gtk-greeter.conf 
file="/etc/lightdm/lightdm-gtk-greeter.conf"
if ! grep -qF "theme-name=Adwaita-dark" "$file"; then
    echo "theme-name=Adwaita-dark" | sudo tee -a "$file" >/dev/null
    echo "indicators= ~spacer;~clock;~spacer;~session;~power" | sudo tee -a "$file" >/dev/null
    echo "clock-format= %H:%M:%S" | sudo tee -a "$file" >/dev/null
    echo "background = /usr/share/pixmaps/Debian_ascii_1920.png" | sudo tee -a "$file" >/dev/null
    echo "icon-theme-name = Papirus-Dark" | sudo tee -a "$file" >/dev/null
    echo "cursor-theme-name = Bibata-Modern-Classic" | sudo tee -a "$file" >/dev/null
    echo "position = 75%,start 50%,center" | sudo tee -a "$file" >/dev/null
    echo "default-user-image = /usr/share/pixmaps/avatar_icone.ico" | sudo tee -a "$file" >/dev/null
    echo "xft-antialias= true" | sudo tee -a "$file" >/dev/null
    echo "xft-dpi= 96" | sudo tee -a "$file" >/dev/null
    echo "xft-hintstyle= hintslight" | sudo tee -a "$file" >/dev/null
    echo "xft-rgba= rgb" | sudo tee -a "$file" >/dev/null
    echo "font-name= Noto Mono 16" | sudo tee -a "$file" >/dev/null
fi
#sudo systemctl restart lightdm


```

[11_Restore_sddm_config.sh](01_Restore/not_install/login_manager/11_Restore_sddm_config.sh)
```bash
#!/bin/bash
# SDDM config



clear

sudo apt install sddm -y --no-install-recommends 

# Autologin sur xfce4
sudo mv /etc/sddm.conf /etc/sddm.conf.old
echo "[Autologin]
User=$USER
Session=xfce.desktop
Relogin=false
" | sudo tee /etc/sddm.conf


# Session=xfce.desktop
# sudo mkdir -p /usr/share/sddm/themes/nonoos
```

 
#### 02_Backup
[Backup_Home.sh](02_Backup/Backup_Home.sh)
```bash
#!/bin/bash

clear

dossierBackup="/mnt/nas/archives/PC/Linux/Backup_Home"

if [ ! -d "$dossierBackup" ]; then
    mkdir -p "$dossierBackup"
fi

if [ ! -f "$dossierBackup/home_$(date +'%d_%m_%Y').tar" ]; then
    rm -rf "$dossierBackup/home_$(date +'%d_%m_%Y').tar"
fi

tar --exclude="$HOME/.cache" --exclude="$HOME/Documents/virtualbox" --exclude="$HOME/.local/share/Trash/info"  --exclude="$HOME/.local/share/Trash/files" --use-compress-program=pigz --sort=name -cvf $dossierBackup/home_$(date +'%d_%m_%Y').tar $HOME
# --exclude="$HOME/.steam"
 sleep 3

num_tar_files=$(ls -lt "$dossierBackup"/*.tar 2>/dev/null | wc -l)
if [ $num_tar_files -gt 3 ]; then
    oldest_tar_file=$(ls -lt "$dossierBackup"/*.tar | awk '{print $NF}' | tail -1)
    sudo rm -rvf "$oldest_tar_file"
fi







# # check si le fichier existe
# file="/home/nono/.config/NonoOS/backup/test.log"
# if [ -e "$file" ]; then
#     modification_time=$(stat -c %Y "$file")
#     current_time=$(date +%s)
#     time_diff=$((current_time - modification_time))
#     three_days=$((3 * 24 * 60 * 60))

#     if [ "$time_diff" -gt "$three_days" ]; then
#         rm "$file"
#         echo "Le fichier $file a été supprimé car il est plus vieux que 3 jours."
#         echo "Lancement de la mise a jour du backup config home"
#         FaireBackup="oui"
#     else
#         echo "La mise a jour s'est faite endéans des 3jours. Pas de mise à jour du backup à faire."
#         FaireBackup="non"
#         echo "faut-il continuer (y/n)"
#         read -r -t 5 -n 1 response  # Wait for 5 seconds for user input
#         echo  # Add newline after user input
#         response=$(echo "$response" | tr '[:upper:]' '[:lower:]')
#         if [[ "$response" =~ ^(y|1|oui|o|yes)$ ]]; then
#                 FaireBackup="oui"
#         else
#             echo "Pas de backup à lancer..."
#         fi
#     fi
# else
#     echo "Le fichier $file n'existe pas."
#     echo "Creation du fichier"
#     touch $file
#     FaireBackup="oui"

# fi


# if [ "$FaireBackup" = "oui" ]; then
    
#     if [ -d "$HOME/.local/share/Trash/" ]; then
#         sudo rm -rf "$HOME"/.local/share/Trash/*
#     fi

# fi
```

 
#### 03_Utilitaires/Applications/mpv
[mpv_single_instance.sh](03_Utilitaires/Applications/mpv/mpv_single_instance.sh)
```bash
#!/bin/bash

#
killall mpv

mpv "$1" &

exit

```

[creation_fichier_readme_et_all.sh](03_Utilitaires/creation_fichier_readme_et_all.sh)
```bash
#!/bin/bash


: << 'COMMENT'
sudo apt install git -y
export GIT_SSL_NO_VERIFY=1 
# ou en perma:
#git config --global http.sslverify false
git clone https://gitlab.com/ardelsaut/debian.git
#git config --global http.sslverify true
cd debian/
sudo ./all_sh.sh
COMMENT

clear

# Trouve tous les fichiers .sh dans un répertoire spécifique, excluant certains sous-répertoires et un fichier spécifique.
sh_files=$(find "$HOME"/Drive/Linux/debian -type f -name "*.sh" ! -path "*not_install*" ! -path "*03_Utilitaires*" ! -path "*02_Backup*" ! -path "*all_commands.sh*" ! -path "*99_en_test*" ! -name "all_sh.sh" | sort)

# Définit le chemin du fichier all_sh.sh
fichier_all_sh="$HOME/Drive/Linux/debian/all_sh.sh"
echo "Création fichier $fichier_all_sh"
# Vérifie si le fichier all_sh.sh existe
if [ -f "$fichier_all_sh" ]; then
    # Si oui, le supprime et crée un nouveau fichier
    sudo rm "$fichier_all_sh"
    touch "$fichier_all_sh"
else
    # Sinon, crée un nouveau fichier
    touch "$fichier_all_sh"
fi

# Pour chaque fichier .sh trouvé, ajoute un commentaire avec le chemin du fichier suivi de son contenu dans all_sh.sh
echo '#!/bin/bash

LOGFILE="/tmp/output.log"
exec > >(tee -a "$LOGFILE") 2>&1
exec 3>&2 2>>"$LOGFILE"
PS4="+ $(date "+%Y-%m-%d %H:%M:%S") [$$] "
set -x' >> "$fichier_all_sh"
echo "" >> "$fichier_all_sh"
echo "" >> "$fichier_all_sh"

for file_path in $sh_files; do
    echo "# $file_path" >> "$fichier_all_sh"
    cat "$file_path" >> "$fichier_all_sh"
    echo "" >> "$fichier_all_sh"  
done

echo 'set +x
exec 2>&3' >> "$fichier_all_sh"
echo "" >> "$fichier_all_sh"
echo 'sudo reboot' >> "$fichier_all_sh"

# Manipulation du contenu de all_sh.sh
# sed -i '1{h;d}; 3{H;x}' "$fichier_all_sh"
chmod +x "$fichier_all_sh"

echo "Création fichier OK!"



# Création de all_commands.sh
# Répète le processus de recherche de fichiers .sh, mais cette fois sans exclure de sous-répertoires.
sh_files=$(find "$HOME"/Drive/Linux/debian -type f -name "*.sh" ! -path "*03_Utilitaires*" ! -path "*not_install*" ! -path "*02_Backup*" ! -path "*99_en_test*" | sort)


# Définit le chemin du fichier all_commands.sh
all_commands="$HOME/Drive/Linux/debian/all_commands.sh"
echo "Création fichier $all_commands"

# Vérifie si le fichier all_commands.sh existe
if [ -f "$all_commands" ]; then
    # Si oui, le supprime et crée un nouveau fichier
    sudo rm "$all_commands"
    touch "$all_commands"
else
    # Sinon, crée un nouveau fichier
    touch "$all_commands"
fi

# Ajoute un en-tête au fichier all_commands.sh
content='#!/bin/bash

original_dir=$(realpath "$(dirname "$0")")
'
echo "$content" >> "$all_commands"

# Pour chaque fichier .sh trouvé, ajoute un commentaire avec le nom du fichier, puis la commande "sh" suivie du chemin relatif du fichier dans all_commands.sh
part_to_remove="$HOME/Drive/Linux/debian/"
for file_path in $sh_files; do
    new_path=$(echo "$file_path" | sed "s|$part_to_remove||")
    file_name=$(basename "$file_path")
    echo "# $file_name" >> "$all_commands"
    echo -n "bash " >> "$all_commands"
    echo -n '"' >> "$all_commands"
    echo -n '$original_dir' >> "$all_commands"
    echo -n "/$new_path" >> "$all_commands"
    echo '"' >> "$all_commands"
    echo "" >> "$all_commands"
done
chmod +x "$all_commands"
echo "Création fichier OK!"


# Création du fichier readme.md
# Répète le processus de recherche de fichiers .sh, cette fois sans exclure un sous-répertoire spécifique.
sh_files=$(find $HOME/Drive/Linux/debian -type f -name "*.sh" ! -path "*99_en_test*" | sort)

# Définit le chemin du fichier readme.md
readme_md="$HOME/Drive/Linux/debian/readme.md"

echo "Création fichier $readme_md"

# Vérifie si le fichier readme.md existe
if [ -f "$readme_md" ]; then
    # Si oui, le supprime et crée un nouveau fichier
    sudo rm "$readme_md"
    touch "$readme_md"
else
    # Sinon, crée un nouveau fichier
    touch "$readme_md"
fi

# Écrit un en-tête dans readme.md
echo "# Regroupement des scripts:" >> "$readme_md"

# Pour chaque fichier .sh trouvé, extrait le nom du fichier et son chemin relatif, puis les ajoute à readme.md
part_to_remove="$HOME/Drive/Linux/debian/"
for file_path in $sh_files; do
    file_name=$(basename "$file_path")
    new_path=$(echo "$file_path" | sed "s|$part_to_remove||")
    parent_dir=$(dirname "$file_path")
    new_path_parent_dir=$(echo "$parent_dir" | sed "s|$part_to_remove||")
    line_to_check="#### $new_path_parent_dir"
    
    # Vérifie si l'en-tête du sous-répertoire existe déjà dans readme.md, sinon l'ajoute
    if ! grep -q "$line_to_check" "$readme_md"; then
        echo " " >> "$readme_md"
        echo "$line_to_check" >> "$readme_md"
    fi
    
    if [[ $file_path == *"all_sh.sh"* ]]; then
        continue
    else
    multiline_text="[$file_name]($new_path)"
    echo "$multiline_text" >> "$readme_md"
    echo '```bash' >> "$readme_md"
    cat "$file_path" >> "$readme_md"
    echo >> "$readme_md"
    echo '```' >> "$readme_md"
    echo >> "$readme_md"    
    fi

done

# Renomme un en-tête spécifique dans readme.md
sed -i 's/^#### \/home\/nono\/Drive\/Linux\/debian$/#### Sommaire/' "$readme_md"

chmod +x "$readme_md"

echo "Création fichier OK!"


# # Auto commit et publication vers git
# ssh-keygen -t rsa -b 4096 -C "ar.delsaut@outlook.com"
# eval "$(ssh-agent -s)"
# ssh-add ~/.ssh/id_rsa
# # git remote set-url origin git@gitlab.com:ardelsaut/debian.git

echo "Mise à jour du dossier dans Git"

cd "$HOME/Drive/Linux/debian"
commit_message="Commit automatique $(date +%d-%m-%y)!"
git add . >> /dev/null
git commit --quiet -m "$commit_message" >> /dev/null
git push --quiet origin main >> /dev/null
cd - >> /dev/null

echo "Dossier dans Git OK!"

```

 
#### 03_Utilitaires/scripts_manuels
[Installer_VirtualBox_Host.sh](03_Utilitaires/scripts_manuels/Installer_VirtualBox_Host.sh)
```bash
#!/bin/bash

clear

sudo apt install fasttrack-archive-keyring -y


line="deb https://fasttrack.debian.net/debian-fasttrack/ bookworm-fasttrack main contrib"
if ! grep -qxF "$line" /etc/apt/sources.list; then
    echo "$line" | sudo tee -a /etc/apt/sources.list >/dev/null
fi

line="deb https://fasttrack.debian.net/debian-fasttrack/ bookworm-backports-staging main contrib"
if ! grep -qxF "$line" /etc/apt/sources.list; then
    echo "$line" | sudo tee -a /etc/apt/sources.list >/dev/null
fi

sudo apt update

sudo apt install binutils binutils-common binutils-x86-64-linux-gnu build-essential dkms dpkg-dev fakeroot g++ g++-12 gcc gcc-12 gnupg gnupg-l10n gnupg-utils gpg-agent gpg-wks-client gpg-wks-server libalgorithm-diff-perl libalgorithm-diff-xs-perl libalgorithm-merge-perl libasan8 libbinutils libc-dev-bin libc-devtools libc6-dev libcc1-0 libcrypt-dev libctf-nobfd0 libctf0 libdpkg-perl libfakeroot libfile-fcntllock-perl libgcc-12-dev libgprofng0 libgsoap-2.8.124 libitm1 liblsan0 liblzf1 liblzo2-2 libnsl-dev libqt5help5 libqt5opengl5 libqt5printsupport5 libqt5sql5 libqt5sql5-sqlite libqt5xml5 libstdc++-12-dev libtirpc-dev libtpms0 libtsan2 libubsan1 libvncserver1 linux-compiler-gcc-12-x86 linux-headers-amd64 linux-kbuild-6.1 linux-libc-dev make manpages-dev rpcsvc-proto virtualbox virtualbox-dkms virtualbox-ext-pack virtualbox-qt virtualbox-guest-x11 --no-install-recommends 
# virtualbox-guest-x11

sudo usermod -aG vboxusers nono

# faire dark theme
desktop_file="/usr/share/applications/virtualbox.desktop"
if [ -f "$desktop_file" ]; then
    sudo sed -i 's/^Exec=VirtualBox %U$/Exec=VirtualBox -style Adwaita-Dark %U/' "$desktop_file"
fi

```

 
#### Sommaire
[all_commands.sh](all_commands.sh)
```bash
#!/bin/bash

original_dir=$(realpath "$(dirname "$0")")

# 00_Restore_Apt_No_Recommends.sh
bash "$original_dir/01_Restore/00_apt_source/00_Restore_Apt_No_Recommends.sh"

# 00_Restore_Apt_Sources_Bookworm.sh
bash "$original_dir/01_Restore/00_apt_source/00_Restore_Apt_Sources_Bookworm.sh"

# 01_ssh.sh
bash "$original_dir/01_Restore/01_ssh/01_ssh.sh"

# 01_Nas.sh
bash "$original_dir/01_Restore/02_Nas/01_Nas.sh"

# 01_Dossier_Config.sh
bash "$original_dir/01_Restore/03_dossier_config/01_Dossier_Config.sh"

# 00_Restore_Wifi.sh
bash "$original_dir/01_Restore/04_autres/00_Restore_Wifi.sh"

# 01_Restore_Audio.sh
bash "$original_dir/01_Restore/04_autres/01_Restore_Audio.sh"

# 03_Restore_polkit-a_No_Password.sh
bash "$original_dir/01_Restore/04_autres/03_Restore_polkit-a_No_Password.sh"

# 04_Restore_Themes.sh
bash "$original_dir/01_Restore/04_autres/04_Restore_Themes.sh"

# 06_Restore_Grub_config.sh
bash "$original_dir/01_Restore/04_autres/06_Restore_Grub_config.sh"

# 07_Restore_Dismiss_DPMS.sh
bash "$original_dir/01_Restore/04_autres/07_Restore_Dismiss_DPMS.sh"

# 08_Restore_Ram_On_File.sh
bash "$original_dir/01_Restore/04_autres/08_Restore_Ram_On_File.sh"

# 09_Restore_Brightness_udev_rules.sh
bash "$original_dir/01_Restore/04_autres/09_Restore_Brightness_udev_rules.sh"

# 09_Restore_Sudo_No_Password.sh
bash "$original_dir/01_Restore/04_autres/09_Restore_Sudo_No_Password.sh"

# 10_Restore_Wallpaper.sh
bash "$original_dir/01_Restore/04_autres/10_Restore_Wallpaper.sh"

# 11_Restore_gmd3.sh
bash "$original_dir/01_Restore/04_autres/11_Restore_gmd3.sh"

# 12_Restore_VirtualBox_guest.sh
bash "$original_dir/01_Restore/04_autres/12_Restore_VirtualBox_guest.sh"

# 00_Packages_Desktop_Gnome.sh
bash "$original_dir/01_Restore/05_Packages/00_Packages_Desktop_Gnome.sh"

# 01_Packages_Desktop_All.sh
bash "$original_dir/01_Restore/05_Packages/01_Packages_Desktop_All.sh"

# 02_Packages_deb_manual.sh
bash "$original_dir/01_Restore/05_Packages/02_Packages_deb_manual.sh"

# 03_Packages_Flatpak.sh
bash "$original_dir/01_Restore/05_Packages/03_Packages_Flatpak.sh"

# 99_Nettoyage_apres_install.sh
bash "$original_dir/01_Restore/05_Packages/99_Nettoyage_apres_install.sh"

# all_commands.sh
bash "$original_dir/all_commands.sh"

# all_sh.sh
bash "$original_dir/all_sh.sh"


```

