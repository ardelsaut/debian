#!/bin/bash

clear

sudo apt install fasttrack-archive-keyring -y


line="deb https://fasttrack.debian.net/debian-fasttrack/ bookworm-fasttrack main contrib"
if ! grep -qxF "$line" /etc/apt/sources.list; then
    echo "$line" | sudo tee -a /etc/apt/sources.list >/dev/null
fi

line="deb https://fasttrack.debian.net/debian-fasttrack/ bookworm-backports-staging main contrib"
if ! grep -qxF "$line" /etc/apt/sources.list; then
    echo "$line" | sudo tee -a /etc/apt/sources.list >/dev/null
fi

sudo apt update

sudo apt install binutils binutils-common binutils-x86-64-linux-gnu build-essential dkms dpkg-dev fakeroot g++ g++-12 gcc gcc-12 gnupg gnupg-l10n gnupg-utils gpg-agent gpg-wks-client gpg-wks-server libalgorithm-diff-perl libalgorithm-diff-xs-perl libalgorithm-merge-perl libasan8 libbinutils libc-dev-bin libc-devtools libc6-dev libcc1-0 libcrypt-dev libctf-nobfd0 libctf0 libdpkg-perl libfakeroot libfile-fcntllock-perl libgcc-12-dev libgprofng0 libgsoap-2.8.124 libitm1 liblsan0 liblzf1 liblzo2-2 libnsl-dev libqt5help5 libqt5opengl5 libqt5printsupport5 libqt5sql5 libqt5sql5-sqlite libqt5xml5 libstdc++-12-dev libtirpc-dev libtpms0 libtsan2 libubsan1 libvncserver1 linux-compiler-gcc-12-x86 linux-headers-amd64 linux-kbuild-6.1 linux-libc-dev make manpages-dev rpcsvc-proto virtualbox virtualbox-dkms virtualbox-ext-pack virtualbox-qt virtualbox-guest-x11 --no-install-recommends 
# virtualbox-guest-x11

sudo usermod -aG vboxusers nono

# faire dark theme
desktop_file="/usr/share/applications/virtualbox.desktop"
if [ -f "$desktop_file" ]; then
    sudo sed -i 's/^Exec=VirtualBox %U$/Exec=VirtualBox -style Adwaita-Dark %U/' "$desktop_file"
fi
