#!/bin/bash


: << 'COMMENT'
sudo apt install git -y
export GIT_SSL_NO_VERIFY=1 
# ou en perma:
#git config --global http.sslverify false
git clone https://gitlab.com/ardelsaut/debian.git
#git config --global http.sslverify true
cd debian/
sudo ./all_sh.sh
COMMENT

clear

# Trouve tous les fichiers .sh dans un répertoire spécifique, excluant certains sous-répertoires et un fichier spécifique.
sh_files=$(find "$HOME"/Drive/Linux/debian -type f -name "*.sh" ! -path "*not_install*" ! -path "*03_Utilitaires*" ! -path "*02_Backup*" ! -path "*all_commands.sh*" ! -path "*99_en_test*" ! -name "all_sh.sh" | sort)

# Définit le chemin du fichier all_sh.sh
fichier_all_sh="$HOME/Drive/Linux/debian/all_sh.sh"
echo "Création fichier $fichier_all_sh"
# Vérifie si le fichier all_sh.sh existe
if [ -f "$fichier_all_sh" ]; then
    # Si oui, le supprime et crée un nouveau fichier
    sudo rm "$fichier_all_sh"
    touch "$fichier_all_sh"
else
    # Sinon, crée un nouveau fichier
    touch "$fichier_all_sh"
fi

# Pour chaque fichier .sh trouvé, ajoute un commentaire avec le chemin du fichier suivi de son contenu dans all_sh.sh
echo '#!/bin/bash

LOGFILE="/tmp/output.log"
exec > >(tee -a "$LOGFILE") 2>&1
exec 3>&2 2>>"$LOGFILE"
PS4="+ $(date "+%Y-%m-%d %H:%M:%S") [$$] "
set -x' >> "$fichier_all_sh"
echo "" >> "$fichier_all_sh"
echo "" >> "$fichier_all_sh"

for file_path in $sh_files; do
    echo "# $file_path" >> "$fichier_all_sh"
    cat "$file_path" >> "$fichier_all_sh"
    echo "" >> "$fichier_all_sh"  
done

echo 'set +x
exec 2>&3' >> "$fichier_all_sh"
echo "" >> "$fichier_all_sh"
echo 'sudo reboot' >> "$fichier_all_sh"

# Manipulation du contenu de all_sh.sh
# sed -i '1{h;d}; 3{H;x}' "$fichier_all_sh"
chmod +x "$fichier_all_sh"

echo "Création fichier OK!"



# Création de all_commands.sh
# Répète le processus de recherche de fichiers .sh, mais cette fois sans exclure de sous-répertoires.
sh_files=$(find "$HOME"/Drive/Linux/debian -type f -name "*.sh" ! -path "*03_Utilitaires*" ! -path "*not_install*" ! -path "*02_Backup*" ! -path "*99_en_test*" | sort)


# Définit le chemin du fichier all_commands.sh
all_commands="$HOME/Drive/Linux/debian/all_commands.sh"
echo "Création fichier $all_commands"

# Vérifie si le fichier all_commands.sh existe
if [ -f "$all_commands" ]; then
    # Si oui, le supprime et crée un nouveau fichier
    sudo rm "$all_commands"
    touch "$all_commands"
else
    # Sinon, crée un nouveau fichier
    touch "$all_commands"
fi

# Ajoute un en-tête au fichier all_commands.sh
content='#!/bin/bash

original_dir=$(realpath "$(dirname "$0")")
'
echo "$content" >> "$all_commands"

# Pour chaque fichier .sh trouvé, ajoute un commentaire avec le nom du fichier, puis la commande "sh" suivie du chemin relatif du fichier dans all_commands.sh
part_to_remove="$HOME/Drive/Linux/debian/"
for file_path in $sh_files; do
    new_path=$(echo "$file_path" | sed "s|$part_to_remove||")
    file_name=$(basename "$file_path")
    echo "# $file_name" >> "$all_commands"
    echo -n "bash " >> "$all_commands"
    echo -n '"' >> "$all_commands"
    echo -n '$original_dir' >> "$all_commands"
    echo -n "/$new_path" >> "$all_commands"
    echo '"' >> "$all_commands"
    echo "" >> "$all_commands"
done
chmod +x "$all_commands"
echo "Création fichier OK!"


# Création du fichier readme.md
# Répète le processus de recherche de fichiers .sh, cette fois sans exclure un sous-répertoire spécifique.
sh_files=$(find $HOME/Drive/Linux/debian -type f -name "*.sh" ! -path "*99_en_test*" | sort)

# Définit le chemin du fichier readme.md
readme_md="$HOME/Drive/Linux/debian/readme.md"

echo "Création fichier $readme_md"

# Vérifie si le fichier readme.md existe
if [ -f "$readme_md" ]; then
    # Si oui, le supprime et crée un nouveau fichier
    sudo rm "$readme_md"
    touch "$readme_md"
else
    # Sinon, crée un nouveau fichier
    touch "$readme_md"
fi

# Écrit un en-tête dans readme.md
echo "# Regroupement des scripts:" >> "$readme_md"

# Pour chaque fichier .sh trouvé, extrait le nom du fichier et son chemin relatif, puis les ajoute à readme.md
part_to_remove="$HOME/Drive/Linux/debian/"
for file_path in $sh_files; do
    file_name=$(basename "$file_path")
    new_path=$(echo "$file_path" | sed "s|$part_to_remove||")
    parent_dir=$(dirname "$file_path")
    new_path_parent_dir=$(echo "$parent_dir" | sed "s|$part_to_remove||")
    line_to_check="#### $new_path_parent_dir"
    
    # Vérifie si l'en-tête du sous-répertoire existe déjà dans readme.md, sinon l'ajoute
    if ! grep -q "$line_to_check" "$readme_md"; then
        echo " " >> "$readme_md"
        echo "$line_to_check" >> "$readme_md"
    fi
    
    if [[ $file_path == *"all_sh.sh"* ]]; then
        continue
    else
    multiline_text="[$file_name]($new_path)"
    echo "$multiline_text" >> "$readme_md"
    echo '```bash' >> "$readme_md"
    cat "$file_path" >> "$readme_md"
    echo >> "$readme_md"
    echo '```' >> "$readme_md"
    echo >> "$readme_md"    
    fi

done

# Renomme un en-tête spécifique dans readme.md
sed -i 's/^#### \/home\/nono\/Drive\/Linux\/debian$/#### Sommaire/' "$readme_md"

chmod +x "$readme_md"

echo "Création fichier OK!"


# # Auto commit et publication vers git
# ssh-keygen -t rsa -b 4096 -C "ar.delsaut@outlook.com"
# eval "$(ssh-agent -s)"
# ssh-add ~/.ssh/id_rsa
# # git remote set-url origin git@gitlab.com:ardelsaut/debian.git

echo "Mise à jour du dossier dans Git"

cd "$HOME/Drive/Linux/debian"
commit_message="Commit automatique $(date +%d-%m-%y)!"
git add . >> /dev/null
git commit --quiet -m "$commit_message" >> /dev/null
git push --quiet origin main >> /dev/null
cd - >> /dev/null

echo "Dossier dans Git OK!"
