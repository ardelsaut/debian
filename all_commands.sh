#!/bin/bash

original_dir=$(realpath "$(dirname "$0")")

# 00_Restore_Apt_No_Recommends.sh
bash "$original_dir/01_Restore/00_apt_source/00_Restore_Apt_No_Recommends.sh"

# 00_Restore_Apt_Sources_Bookworm.sh
bash "$original_dir/01_Restore/00_apt_source/00_Restore_Apt_Sources_Bookworm.sh"

# 01_ssh.sh
bash "$original_dir/01_Restore/01_ssh/01_ssh.sh"

# 01_Nas.sh
bash "$original_dir/01_Restore/02_Nas/01_Nas.sh"

# 01_Dossier_Config.sh
bash "$original_dir/01_Restore/03_dossier_config/01_Dossier_Config.sh"

# 00_Restore_Wifi.sh
bash "$original_dir/01_Restore/04_autres/00_Restore_Wifi.sh"

# 01_Restore_Audio.sh
bash "$original_dir/01_Restore/04_autres/01_Restore_Audio.sh"

# 03_Restore_polkit-a_No_Password.sh
bash "$original_dir/01_Restore/04_autres/03_Restore_polkit-a_No_Password.sh"

# 04_Restore_Themes.sh
bash "$original_dir/01_Restore/04_autres/04_Restore_Themes.sh"

# 06_Restore_Grub_config.sh
bash "$original_dir/01_Restore/04_autres/06_Restore_Grub_config.sh"

# 07_Restore_Dismiss_DPMS.sh
bash "$original_dir/01_Restore/04_autres/07_Restore_Dismiss_DPMS.sh"

# 08_Restore_Ram_On_File.sh
bash "$original_dir/01_Restore/04_autres/08_Restore_Ram_On_File.sh"

# 09_Restore_Brightness_udev_rules.sh
bash "$original_dir/01_Restore/04_autres/09_Restore_Brightness_udev_rules.sh"

# 09_Restore_Sudo_No_Password.sh
bash "$original_dir/01_Restore/04_autres/09_Restore_Sudo_No_Password.sh"

# 10_Restore_Wallpaper.sh
bash "$original_dir/01_Restore/04_autres/10_Restore_Wallpaper.sh"

# 11_Restore_gmd3.sh
bash "$original_dir/01_Restore/04_autres/11_Restore_gmd3.sh"

# 12_Restore_VirtualBox_guest.sh
bash "$original_dir/01_Restore/04_autres/12_Restore_VirtualBox_guest.sh"

# 00_Packages_Desktop_Gnome.sh
bash "$original_dir/01_Restore/05_Packages/00_Packages_Desktop_Gnome.sh"

# 01_Packages_Desktop_All.sh
bash "$original_dir/01_Restore/05_Packages/01_Packages_Desktop_All.sh"

# 02_Packages_deb_manual.sh
bash "$original_dir/01_Restore/05_Packages/02_Packages_deb_manual.sh"

# 03_Packages_Flatpak.sh
bash "$original_dir/01_Restore/05_Packages/03_Packages_Flatpak.sh"

# 99_Nettoyage_apres_install.sh
bash "$original_dir/01_Restore/05_Packages/99_Nettoyage_apres_install.sh"

# all_commands.sh
bash "$original_dir/all_commands.sh"

# all_sh.sh
bash "$original_dir/all_sh.sh"

